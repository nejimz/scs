@extends('layouts.app')

@section('content')
<div class="row">
    <div class="small-12 columns">
    <h1 class="text-center">Shiloh Christian School Inventory Management</h1>
    <p class="text-center">Your Application's Landing Page.</p>
    </div>
</div>
<div class="row" >
<div class="small-12 columns text-center"> 
  @include('layouts.message')
</div>
</div>
@endsection
