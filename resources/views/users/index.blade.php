@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
  <div class="row" >
    <div class="medium-12 columns">
      <h4>Users</h4>
    </div>
  </div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
  <div class="row" >
    <div class="medium-12 columns">
      <form action="" method="get">
        <div class="row">
          <div class="medium-4 columns">
            <div class="input-group">
              <input class="input-group-field" type="text" name="search" value="{{ $search }}">
              <div class="input-group-button">
                <button type="submit" class="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="medium-8 columns">&nbsp;</div>
        </div>
      </form>
      <table class="hover">
        <thead>
          <th width="5%"><a href="{{ route('user_create') }}" title="Add User"><i class="fa fa-plus"></i></a></th>
          <th width="5%">ID</th>          
          <th width="10%">Username</th>
          <th width="40%">Name</th>
          <th width="10%" align="center">Penalties</th>
          <th width="2%">Admin</th>
          <th width="2%">Active</th>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td><a href="{{ route('user_edit', $user->id) }}" title="Edit"><i class="fa fa-edit"></i></a></td>
            <td>{{ $user->id }}</td>            
            <td>{{ $user->email }}</td>
            <td>{{ $user->name }}</td>
            <td align="center"><a href="{{ route('user_penalties', $user->id) }}">{{ $user->total_penalties }}</a></td>
            <td align="center">{!! $user->role_admin_label !!}</td>
            <td align="center">{!! $user->active_status_label !!}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
        {!! $users->render() !!}
    </div>
  </div>
  <!-- Container Content -->
        
<!-- Stops Container -->
@stop