@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
  <div class="row" >
    <div class="medium-12 columns">
      <h4><a href="{{ route('users') }}">Users</a> Penlaties</h4>
    </div>
  </div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
  <div class="row" >
    <div class="medium-12 columns">
      <table class="hover">
        <thead>
          <th width="20%">ISBN</th>          
          <th width="30%">Name</th>
          <th width="20%">Brand</th>
          <th width="20%">Category</th>
          <th width="10%">Status</th>
        </thead>
        <tbody>
          @foreach($rows as $row)
          <tr>
            <td>{{ $row->isbn }}</td>            
            <td>{{ $row->item_name }}</td>
            <td>{{ $row->brand_name }}</td>
            <td>{!! $row->category_name !!}</td>
            <td>{!! $row->status_name !!}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
        {!! $rows->render() !!}
    </div>
  </div>
  <!-- Container Content -->
        
<!-- Stops Container -->
@stop