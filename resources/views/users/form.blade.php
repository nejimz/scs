@extends('layouts.app')

@section('content')
<!-- Starts Container -->
  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>
  <!-- Container Title -->
  <div class="row" >
   
      @if(!Request::is('portal/user/*/edit'))
          <div class="small-4 small-centered columns">
              <h4>New User</h4>
          </div>
      @else
          <div class="small-12 small-centered columns">
              <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                  <li style="font-size: 12pt;"><a href="{{ route('users') }}">Users</a></li>
                  <li style="font-size: 12pt;">
                    <span class="show-for-sr">Current: </span> Edit
                  </li>
                </ul>
              </nav>
          </div>
      @endif
  </div>
  <!-- Container Title -->
<form action="{{ $action }}" method="post">
  <!-- Container Content -->
@if(!Request::is('portal/user/*/edit'))
     @include('users.profile.account')  
@else
<ul class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs id="deeplinked-tabs">
  <li class="tabs-title is-active"><a href="#personal" aria-selected="true">Personal Information</a></li>
  <li class="tabs-title"><a href="#account">Account Settings</a></li>
</ul>
  <div class="tabs-content" data-tabs-content="deeplinked-tabs" style="margin-bottom:20px;" >
    <div class="tabs-panel is-active" id="personal">
      <p>
         @include('users.profile.personal')
      </p>
    </div>
    <div class="tabs-panel" id="account">
      <p>
        @include('users.profile.account')     
      </p>
    </div>
  </div>
@endif   
  <div class="row">
    <div class="small-2 small-centered columns">
      <button class="button primary" name="save" value="true">Save</button>
      <a href="{{ route('users') }}" class="button secondary">Back</a>
    </div>
  </div>
  {!! csrf_field() !!}
</form>

  <!-- Container Content -->
<script type="text/javascript">
</script>
<!-- Stops Container -->
@stop