<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-2 columns">
            <label for="name" class="text-right">Name</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="name" name="name" value="{{ $name }}">
          </div>
        </div>

        <div class="row">
          <div class="small-2 columns">
            <label for="email" class="text-right">Username</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="email" name="email" value="{{ $email }}">
          </div>
        </div>

        @if(!Request::is('portal/user/*/edit'))
        <div class="row">
          <div class="small-2 columns">
            <label for="email" class="text-right">Password</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="password" name="password" readonly="true" style="font-family: 'Lucida Console', 'Lucida Sans Typewriter', monaco, 'Bitstream Vera Sans Mono', monospace;" value="{{  str_random(6) }}">
          </div>
        </div>
        @else
        <div class="row">
          <div class="small-2 columns">
            <label for="email" class="text-right">Password</label>
          </div>
          <div class="small-10 columns">
            <input type="password" id="password" name="password">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">
            <label for="password_confirmation" class="text-right">Confirm Password</label>
          </div>
          <div class="small-10 columns">
            <input type="password" id="password_confirmation" name="password_confirmation">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns"> </div>
          <div class="small-10 columns">
            <div class="input-group">       
                <a href="{{ route('user_edit', $user_id) }}" class="input-group-label"><i class="fa fa-refresh"></i></a>
              <input class="input-group-field" type="text" id="password_reset" name="password_reset" readonly="true" style="font-family: 'Lucida Console', 'Lucida Sans Typewriter', monaco, 'Bitstream Vera Sans Mono', monospace;" value="{{  old('password_reset', str_random(6)) }}">
              <div class="input-group-button">
                <button class="button warning" name="reset" value="true"> Reset </button>
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="row">
          <div class="small-8 small-centered columns">
            <div class="card">
                <div class="card-divider">
                  <strong>Role</strong>
                </div>
                <div class="card-section">
                  <?php 
                    $role = isset($role) ? $role : 'basic'; 
                    $active = isset($active) ? $active : 0;
                  ?>
                  <label><input type="radio" name="role" value="admin" {{ $role == 'admin' ? 'checked' : '' }}> Administrator</label>
                  <label><input type="radio" name="role" value="basic" {{ $role == 'basic' ? 'checked' : '' }}> Basic</label>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">
            <label for="Active" class="text-right">Active</label>
          </div>
          <div class="small-10 columns">
            <div class="switch large">
              <input type="hidden" name="active" value="0">
              <input class="switch-input" id="yes-no" type="checkbox" {{ ($active == 1) ? 'checked' : '' }} value="1" name="active">
              <label class="switch-paddle" for="yes-no">
                <span class="show-for-sr">Active</span>
                <span class="switch-active" aria-hidden="true">Yes</span>
                <span class="switch-inactive" aria-hidden="true">No</span>
              </label>
            </div>
          </div>
        </div>
    </div>
  </div>