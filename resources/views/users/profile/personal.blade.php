@can('admin-access')
<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="position" class="text-right">Position</label>
          </div>
          <div class="small-9 columns">
            <select id="position" name="position_id">
            	<option value=''>-- Select Postion --</option>
            	@foreach($positions as $position)
            		<option value="{{ $position->id }}">{{ $position->name }}</option>
            	@endforeach
            </select>
          </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="school_id" class="text-right">School ID</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="school_id" name="school_id" value="{{ $school_id or '' }}">
          </div>
        </div>
    </div>
</div>
@endcan

@can('basic-access')
<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="school_id" class="text-right">School ID</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="school_id" read-only name="school_id" value="{{ $school_id or '' }}">
          </div>
        </div>
    </div>
</div>
@endcan

<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="first_name" class="text-right">First Name</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="first_name" name="first_name" value="{{ $first_name or '' }}">
          </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="middle_name" class="text-right">Middle Name</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="middle_name" name="middle_name" value="{{ $middle_name or '' }}">
          </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="last_name" class="text-right">Last Name</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="last_name" name="last_name" value="{{ $last_name or '' }}">
          </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="address" class="text-right">Address</label>
          </div>
          <div class="small-9 columns">
            <textarea id="address" name="address">{{ $address or '' }}</textarea>
          </div>
        </div>
    </div>
</div>

<!-- <div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="date_of_birth" class="text-right">Date of Birth</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="date_of_birth" name="date_of_birth" value="{{ $date_of_birth or '' }}">
          </div>
        </div>
    </div>
</div> -->
@if(isset($user_position))
@if($user_position != 0)
  @if($user_position < 2)
  <hr>

  <div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="grade" class="text-right">Grade</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="grade" name="grade" value="{{ $grade or '' }}">
          </div>
        </div>
    </div>
  </div>


  <div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="section" class="text-right">Section</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="section" name="section" value="{{ $section or '' }}">
          </div>
        </div>
    </div>
  </div>
  @else
  <hr>
  <!-- <div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="date_hired" class="text-right">Date Hired</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="date_hired" name="date_hired" value="{{ $date_hired or '' }}">
          </div>
        </div>
    </div>
  </div> -->

  <div class="row" >
    <div class="small-6 small-centered columns">
        <div class="row">
          <div class="small-3 columns">
            <label for="educational_background" class="text-right">Educational Background</label>
          </div>
          <div class="small-9 columns">
           <textarea id="educational_background" name="educational_background">{{ $educational_background or '' }}</textarea>
          </div>
        </div>
    </div>
  </div>
  @endif
@endif
@endif

<script type="text/javascript">
	$('#position').val('{{ $user_position or ""}}');
</script>


