<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ $title or "Shiloh Church School Inventory Management and Decision Support System" }}</title>
    <!-- STYLESHEETS -->
     <link rel="stylesheet" type="text/css" href="{{ asset('foundation-sites/dist/css/foundation.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('foundation-datepicker/css/foundation-datepicker.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/themes/smoothness/jquery-ui.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('default/css/style.css') }}" />
    <!-- STYLESHEETS -->    
    <script src="{{ asset('jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('highcharts/highcharts.js') }}"></script>

    <script type="text/javascript" src="{{ asset('noty/js/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
    <script>
        function notify(message) 
        {
          noty({
              layout: 'bottomLeft', theme: 'metroui', type: 'information', text: message, timeout: 3000, progressBar: true
          });
        }

        function notification_interval()
        {
          //$('span.notification').hide();
          setInterval(function(){
            $.getJSON( "{{ route('request_notification') }}", function( request ) {
              if(request.service_request.length>0){
                $('span.notification').show().text(request.service_request.length);
              }else{
                $('span.notification').hide();
              }
            });
          }, 3000);
        }
    </script>
</head>
<body id="app-layout">
 <div class="top-bar">
      <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
          <li class="menu-text">
            <a href="{{ url('/') }}" style="padding:0px; color:black;">{{ config()->get('app.name') }}</a>
          </li>
          <li><a href="{{ url('/home') }}">Home</a></li>
          @if(Auth::guest())

          @else
            <li>
              <a href="{{ route('service_request') }}">Request
              @can('admin-access')
                <?php $req_count = \App\ServiceRequest::with('user')->whereRequestStatusId(1)->orderBy('request_datetime', 'ASC')->get()->count(); ?>
                @if($req_count <= 0)
                  <span class="badge alert notification" style="display:none !important;">
                @else
                  <span class="badge alert notification">
                @endif
                {{ $req_count }}
                </span>
              @endcan
              </a>
            </li>
            @can('admin-access') <!-- ROLES (admin or basic) -->
              <li><a href="{{ route('item') }}">Items</a></li>
              <li><a href="{{ route('item_brand') }}">Brand</a></li>
              <li><a href="{{ route('item_category') }}">Category</a></li>
              <li><a href="{{ route('users') }}">Users</a></li>
              <li><a href="{{ route('reports') }}">Reports</a></li>
            @endcan          
          @endif
        </ul>
      </div>
      <div class="top-bar-right">
        <ul class="menu">
            <ul class="dropdown menu" data-dropdown-menu>
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown menu" data-dropdown-menu>
                      <a href="#">
                          {{ Auth::user()->name }} 
                          <span class="caret"></span>
                      </a>
                      <ul id="right-vertical-nav" class="menu vertical">
                          <li><a href="{{ url('/profile') }}">My Account</a></li>
                          <li><a href="{{ url('/logout') }}">Logout</a></li>
                      </ul>
                    </li>
                @endif
            </ul>
          </ul>
      </div>
    </div>
    
    <div class="row" style="margin-top:30px;">
      <div class="small-12 columns">
        <div class="card">
          <div class="card-section">
            @yield('content')
          </div>          
        </div>
      </div>
    </div>    

    <footer id="footer">
      <hr>
      <center>
       {{ config()->get('app.name') }} &copy; {{ date('Y') }}
      </center>        
    </footer>
    <!-- SCRIPTS -->
      <script src="{{ asset('what-input/dist/what-input.min.js') }}"></script>
      <script src="{{ asset('foundation-sites/dist/js/foundation.min.js') }}"></script>
      <script src="{{ asset('foundation-datepicker/js/foundation-datepicker.min.js') }}"></script>
      <script>
        $(document).foundation();
         $(window).bind("load", function () {
            var footer = $("#footer");
            var pos = footer.position();
            var height = $(window).height();
            height = height - pos.top;
            height = height - footer.height();
            if (height > 0) {
                footer.css({
                    'margin-top': height + 'px'
                });
            }
        });
         $(function(){
          $('.defaultpicker').fdatepicker({
            disableDblClickSelection: true,
            language: 'vi'
          });

          $('.ddpicker').fdatepicker({
            format: 'yyyy-mm-dd',
            disableDblClickSelection: true,
            language: 'vi',
            pickTime: false
          });

          $('.dtpicker').fdatepicker({
            format: 'yyyy-mm-dd hh:ii',
            disableDblClickSelection: true,
            language: 'vi',
            pickTime: true
          });

         });

         // implementation of disabled form fields
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#report_date_start').fdatepicker({
          onRender: function (date) {
            return '';
          }
        }).on('changeDate', function (ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.update(newDate);
          }
          checkin.hide();
          $('#report_date_end')[0].focus();
        }).data('datepicker');
        var checkout = $('#report_date_end').fdatepicker({
          onRender: function (date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function (ev) {
          checkout.hide();
        }).data('datepicker');
         notification_interval();


         $(function(){
          var nowTemp = new Date();
          var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
          var checkin = $('.dpd1').fdatepicker({
            format: 'yyyy-mm-dd hh:ii',
            disableDblClickSelection: true,
            language: 'vi',
            pickTime: true,
            onRender: function (date) {
              return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
          }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
              var newDate = new Date(ev.date)
              newDate.setDate(newDate.getDate() + 1);
              checkout.update(newDate);
            }
            checkin.hide();
            $('.dpd2')[0].focus();
          }).data('datepicker');
          var checkout = $('.dpd2').fdatepicker({
            format: 'yyyy-mm-dd hh:ii',
            disableDblClickSelection: true,
            language: 'vi',
            pickTime: true,
            onRender: function (date) {
              return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
          }).on('changeDate', function (ev) {
            checkout.hide();
          }).data('datepicker');
         });
      </script>
    <!-- SCRIPTS --> 
  </body>
</html>
