  <div class="row" >
    <div class="small-12 columns">
		@if (count($errors) > 0)
			<div class="callout alert">
				<ul class="no-bullet">
			        @foreach ($errors->all() as $error)
			        	<li>{{ str_replace('email', 'username', $error) }}</li>	           
			        @endforeach
			    </ul>
			</div>
		@endif
		@if(Session::has('custom_error'))
			<div class="callout alert">
				{{ Session::get('custom_error') }}
			</div>
		@endif
		@if(Session::has('success'))
			<div class="callout success">
				{{ Session::get('success') }}
			</div>
		@endif
		@if(isset($high_demand_items))
			@if(count($high_demand_items) > 0)
				<div class="callout alert">
					<i class="fa fa-exclamation-circle"></i> Due to demand some item/s are in need of purchase.  <a href="{{ route('puchase_dss') }}"> See link for details.</a> 
				</div>
			@endif
		@endif
	</div>
 </div>