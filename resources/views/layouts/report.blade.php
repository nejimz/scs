<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ $title or "Shiloh Church School Inventory Management and Decision Support System" }}</title>
    <!-- STYLESHEETS -->
     <link rel="stylesheet" type="text/css" href="{{ asset('foundation-sites/dist/css/foundation.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('foundation-datepicker/css/foundation-datepicker.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/themes/smoothness/jquery-ui.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('default/css/style.css') }}" />
    <!-- STYLESHEETS -->    
    <script src="{{ asset('jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
</head>
<body id="app-layout">
    <div class="row" style="margin-top:30px;">
      <div class="small-12 columns">
            @yield('content')
      </div>
    </div>    

    <footer id="footer">
      <hr>
      <center>
       {{ config()->get('app.name') }} &copy; {{ date('Y') }}
      </center>        
    </footer>
    <!-- SCRIPTS -->
      <script src="{{ asset('what-input/dist/what-input.min.js') }}"></script>
      <script src="{{ asset('foundation-sites/dist/js/foundation.min.js') }}"></script>
    <!-- SCRIPTS --> 
  </body>
  <style type="text/css">
    body { 
        zoom: 0.9; 
        -moz-transform: scale(0.9); 
        -moz-transform-origin: 0 0;
        position: relative; left: 5%; top: 0%;
    } 
  </style>
</html>
