@extends('layouts.app')

@section('content')
<div class="row">
  <div class="medium-6 medium-centered large-4 large-centered columns">
      <form role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <div class="row column log-in-form">
       <h4 class="text-center">Login</h4>
       @include('layouts.message')
        <label>Username
          <input id="email" type="text" placeholder="Username" name="email" value="{{ old('email') }}">
        </label>
        <label>Password
          <input id="password" type="password" placeholder="Password" name="password">
        </label>
        <p><button type="submit" class="button expanded">Log In</button></p>
        <!-- <a class="text-center" href="{{ url('/password/reset') }}">Forgot Your Password?</a> -->
      </div>
    </form>
  </div>
</div>
@endsection
