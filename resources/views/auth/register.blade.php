@extends('layouts.app')

@section('content')
<div class="row">
  <div class="medium-6 medium-centered large-4 large-centered columns">
      <form role="form" method="POST" action="{{ url('/register') }}">
      {{ csrf_field() }}
      <div class="row column log-in-form">
       <h4 class="text-center">Register</h4>
       @include('layouts.message')
       <label>Name
          <input id="name" type="text" placeholder="Name" name="name" value="{{ old('name') }}">
        </label>
        <label>Username
          <!-- <input id="email" type="email" placeholder="Email" name="email" value="{{ old('email') }}"> -->
          <input id="email" type="text" placeholder="Username" name="email" value="{{ old('email') }}">
        </label>
        <label>Password
          <input id="password" type="password" placeholder="Password" name="password">
        </label>
        <label>Confirm Password
          <input id="password-confirm" type="password" placeholder="Confirm Password" name="password_confirmation">
        </label>
        <p><button type="submit" class="button expanded">Register</button></p>
      </div>
    </form>
  </div>
</div>
@endsection
