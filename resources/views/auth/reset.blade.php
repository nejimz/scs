@extends('layouts.app')

@section('content')
<div class="row">
  <div class="medium-6 medium-centered large-4 large-centered columns">
      <form role="form" method="POST" action="{{ route('reset_user_confirm') }}">
      {{ csrf_field() }}
      <div class="row column log-in-form">
       <h4 class="text-center">Reset Password</h4>
       @include('layouts.message')
        <label>New Password
          <input id="password" type="password" placeholder="Password" name="password">
        </label>
        <label>Confirm Password
          <input id="password" type="password" placeholder="Confirm Password" name="password_confirmation">
        </label>
        <p><button type="submit" class="button expanded">Reset</button></p>
        <!-- <a class="text-center" href="{{ url('/password/reset') }}">Forgot Your Password?</a> -->
      </div>
    </form>
  </div>
</div>
@endsection
