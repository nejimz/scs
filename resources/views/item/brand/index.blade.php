@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
  <div class="row" >
    <div class="medium-12 columns">
      <h4>Item Brand</h4>
    </div>
  </div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
  <div class="row" >
    <div class="medium-12 columns">
      <table>
      	<thead>
      		<th width="5%"><a href="{{ route('item_brand_create') }}" title="Add Brand"><i class="fa fa-plus"></i></a></th>
      		<th width="95%">Name</th>
      	</thead>
      	<tbody>
      		@foreach($rows as $row)
      		<tr>
      			<td><a href="{{ route('item_brand_edit', $row->id) }}" title="Edit"><i class="fa fa-edit"></i></a></td>
      			<td>{{ $row->name }}</td>
      		</tr>
      		@endforeach
      	</tbody>
      </table>
    </div>
  </div>
  <!-- Container Content -->
        
<!-- Stops Container -->
@stop