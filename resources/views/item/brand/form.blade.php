@extends('layouts.app')

@section('content')
<!-- Starts Container -->

  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>

  <!-- Container Title -->
  <div class="row" >
    <div class="small-4 small-centered columns">
      <h4>New Item Brand</h4>
    </div>
  </div>
  <!-- Container Title -->

  <!-- Container Content -->
  <div class="row" >
    <div class="small-6 small-centered columns">
      <form action="{{ $action }}" method="post">
        <div class="row">
          <div class="small-2 columns">
            <label for="name" class="text-right">Name</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="name" name="name" value="{{ $name }}">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">&nbsp;</div>
          <div class="small-10 columns">
            <button class="button primary"> Save</button>
            <a href="{{ route('item_brand') }}" class="button secondary">Back</a>
          </div>
        </div>
        {!! csrf_field() !!}
      </form>
    </div>
  </div>
  <!-- Container Content -->
        
<!-- Stops Container -->
@stop