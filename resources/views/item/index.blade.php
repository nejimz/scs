@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
  <div class="row" >
    <div class="medium-12 columns">
      <h4>Item</h4>
    </div>
  </div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
  <div class="row" >
    <div class="medium-12 columns">
      <form action="" method="get">
        <div class="row">
          <div class="medium-4 columns">
            <div class="input-group">
              <input class="input-group-field" type="text" name="search" value="{{ $search }}">
              <div class="input-group-button">
                <button type="submit" class="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="medium-8 columns">&nbsp;</div>
        </div>
      </form>
      <table class="hover">
        <thead>
          <th width="3%">
            <a href="{{ route('item_create') }}" title="Add Item"><i class="fa fa-plus"></i></a>
          </th>
          <th width="10%">QR Code</th>
          <th width="17%">ISBN</th>
          <th width="40%">Name</th>
          <th width="15%">Brand</th>
          <th width="15%">Category</th>
        </thead>
        <tbody>
          @foreach($items as $item)
          <tr>
            <td>
              <a href="{{ route('item_edit', $item->id) }}" title="Edit"><i class="fa fa-edit"></i></a>
              <!-- <a href="#" title="History"><i class="fa fa-history"></i></a> -->
            </td>
            <td><a href="{{ route('item_qrprint', $item->id) }}" target="_blank">
            <img src='data:image/png;base64,{{ DNS2D::getBarcodePNG($item->isbn, "QRCODE",3,3) }}' alt="barcode"/>
            </a></td>
            <td>{{ $item->isbn }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->brand->name }}</td>
            <td>{{ $item->category->name }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
        {!! $items->render() !!}
    </div>
  </div>
  <!-- Container Content -->
        
<!-- Stops Container -->
@stop