@extends('layouts.app')

@section('content')
<!-- Starts Container -->
  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>

  <!-- Container Title -->
  <div class="row" >
    <div class="small-4 small-centered columns">
      @if(Request::is('portal/item/*/edit'))
      <h4>Edit Item</h4>
      @else
      <h4>New Item</h4>
      @endif
    </div>
  </div>
  <!-- Container Title -->

  <!-- Container Content -->
  <div class="row" >
    <div class="small-6 small-centered columns">
      <form action="{{ $action }}" method="post">
       @if(Request::is('portal/item/*/edit') && isset($item_id))
        <div class="row">
          <div class="small-8 small-centered columns">
            <a href="{{ route('item_qrprint', $item_id) }}" target="_blank">
            <img src='data:image/png;base64,{{ DNS2D::getBarcodePNG($isbn, "QRCODE",5,5) }}' alt="barcode"/>
            </a>
          </div>
        </div>
        <div class="row" style="margin-bottom: 10px;">
          <div class="small-8 small-centered columns"><em>(Click image to print)</em></div>
        </div>
        @endif
        <div class="row">
          <div class="small-2 columns">
            <label for="isbn" class="text-right">ISBN</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="isbn" name="isbn" value="{{ $isbn }}">
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="name" class="text-right">Name</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="name" name="name" value="{{ $name }}">
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="item_brand_id" class="text-right">Brand</label>
          </div>
          <div class="small-10 columns">
            <select id="item_brand_id" name="item_brand_id">
              <option value=""></option>
              @foreach($brand as $row)
                <option value="{{ $row->id }}">{{ $row->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="item_category_id" class="text-right">Category</label>
          </div>
          <div class="small-10 columns">
            <select id="item_category_id" name="item_category_id">
              <option value=""></option>
              @foreach($category as $row)
                <option value="{{ $row->id }}">{{ $row->name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="small-2 columns">&nbsp;</div>
          <div class="small-10 columns">
            <button class="button primary"> Save</button>
            <a href="{{ route('item') }}" class="button secondary">Back</a>
          </div>
        </div>
        {!! csrf_field() !!}
      </form>
    </div>
  </div>
  <!-- Container Content -->
<script type="text/javascript">

$('#item_brand_id').val('<?php echo $item_brand_id; ?>');
$('#item_category_id').val('<?php echo $item_category_id; ?>');
</script>
<!-- Stops Container -->
@stop