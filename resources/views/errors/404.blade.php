@extends('layouts.app')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <div class="card" style="margin:0px;">
            <div class="card-divider">
                <h4>Trying to get where you want to go?</h4>
              </div>
              <h1 class="text-center">404</h1>
              <h3 class="text-center">Page not found.</h3>
              <div class="card-section">
                <p class="text-center"><a href="{{ route('dashboard') }}">Return home.</a></p>
              </div>
        </div>
    </div>
</div>
<style type="text/css">
    h1{
        font-size: 200pt !important;
        margin-bottom: 0px;
    }
    h3{
        margin-top:-50px !important;
    }
</style>
@endsection
