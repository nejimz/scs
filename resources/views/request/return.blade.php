@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
<div class="row" >
    <div class="medium-10 columns">
    	<h4><a href="{{ route('service_request') }}">Service Request</a> # <u>{{ $id }}</u></h4>
    </div>
     <div class="medium-2 columns text-right">
     	<a class="button" target="_blank" href="{{ route('service_request_receipt', $id) }}"> <i class="fa fa-print"></i> Print</a>
     </div>
</div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
<div class="row">
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-5 columns">Requester</div>
		    <div class="medium-7 columns">{{ $sr->user->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Request Datetime</div>
		    <div class="medium-7 columns">{{ $sr->request_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Return Datetime</div>
		    <div class="medium-7 columns">{{ $sr->return_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Status</div>
		    <div class="medium-7 columns">{{ $sr->request_status->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Date Created</div>
		    <div class="medium-7 columns">{{ $sr->created_at }}</div>
		</div>
	</div>
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-2 columns">Remarks</div>
		    <div class="medium-10 columns">{{ $sr->remarks }}</div>
		</div>
	</div>
</div>

<div class="row" >
    <div class="medium-12 columns">
      	<table class="hover">
	        <thead>
				<th width="5%"></th>
				<th width="35%">Brand</th>
				<th width="35%">Item/s</th>
				<th width="10%">Quantity</th>
				<th width="15%">Status</th>
	        </thead>
	        <tbody>
	        	@foreach($sr_rows as $row)
	        		<tr>
	        			<td>
	        				@if($row->request_brand_status_id == 2)
	        				<a href="{{ route('service_request_closed', $row->id) }}" title="Close this Service Request?"><i class="fa fa-times"></i></a>
	        				@endif
	        			</td>
	        			<td>{{ $row->brand->name }}</td>
	        			<td>
	        				<ol>
	        				@foreach(App\ServiceRequestBorrowedItem::whereRequestItemId($row->id)->get() as $v)
	        					<li><p>
	        						<span><img src='data:image/png;base64,{{ DNS2D::getBarcodePNG(route("service_request_return_item", $v->id), "QRCODE",3,3) }}' alt="barcode"/></span>
	        						@if(App\ServiceRequestReturnItem::where('request_borrowed_item_id', $v->id)->count() == 0)
	        							<a href="{{ route('service_request_return_item', $v->id) }}" title="Item return">{{ $v->item->name }}</a>
	        						@else
	        							{{ $v->item->name }}
	        						@endif
	        					</p></li>
	        				@endforeach
	        				</ol>
	        			</td>
	        			<td>{{ $row->quantity }}</td>
	        			<td>{{ $row->request_item_status->name }}</td>
	        		</tr>
	        	@endforeach
	        </tbody>
      	</table>
    </div>
</div>
<!-- Container Content -->
<script type="text/javascript">
function confirmation(url, caption)
{
	if(confirm('Are you sure to ' + caption + ' this item?') == true)
	{
		window.location.replace(url);
		return true;
	}
}
</script>
<!-- Stops Container -->
@stop