@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
<style type="text/css">

.legend-pending
{
	background-color:#cccccc!important;
}
.legend-approved
{
	background-color:#77dd77!important;
}
.legend-closed
{
	background-color:#b0b0ff!important;
}
.legend-denied
{
	background-color:#ffb0b0!important;
}
.legend-cancelled
{
	background-color:#dd77dd!important;
}
</style>

<div class="row" >
    <div class="medium-12 columns">
    	<h4>Service Request</h4>
    </div>
</div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
<div class="row" >
    <div class="medium-12 columns">
	    <div class="row">
	      	<div class="medium-5 columns">
				<form action="" method="get">
			        <div class="row">
			        	<div class="medium-8 columns">
			            	<select id="status" name="status">
			            		<option value="">All</option>
			            		@foreach(\App\ServiceRequestStatus::get() as $row)
			            			<option value="{{ $row->id }}">{{ $row->name }}</option>
			            		@endforeach
			            	</select>
			        	</div>
			        	<div class="column-4 columns">&nbsp;</div>
			        </div>
			        <div class="row">
			        	<div class="medium-6 columns">
			            	<input class="input-group-field" type="text" name="search" value="{{ $search }}">
			        	</div>
			        	<div class="medium-2 columns">
			            	<button type="submit" class="button"><i class="fa fa-search"></i></button>
			        	</div>
			        	<div class="column-4 columns">&nbsp;</div>
			        </div>

			        <!--div class="input-group">
			        	<input class="input-group-field" type="text" name="search" value="{{ $search }}">
			        	<div class="input-group-button">
			            	<button type="submit" class="button"><i class="fa fa-search"></i></button>
			        	</div>
			        </div-->
				</form>
			</div>
			<div class="medium-7 columns">
				<ul class="no-bullet">
					<li><strong>Legend</strong></li>
					@foreach($legend as $v)
						@if($v->name == 'Approved')
							<li class="legend-approved">{{ $v->name }}</li>
						@elseif($v->name == 'Pending')
							<li class="legend-pending">{{ $v->name }}</li>
						@elseif($v->name == 'Closed')
							<li class="legend-closed">{{ $v->name }}</li>
						@elseif($v->name == 'Denied')
							<li class="legend-denied">{{ $v->name }}</li>
						@elseif($v->name == 'Cancelled')
							<li class="legend-cancelled">{{ $v->name }}</li>
						@endif
					@endforeach
				</ul>
			</div>
	    </div>
      	<table class="hover">
	        <thead>
	          <th width="8%">
	            <a href="{{ route('service_request_create') }}" title="Add Item"><i class="fa fa-plus"></i></a>
	          </th>
	          <th width="10%">Request #</th>
	          <th width="30%">Requester</th>
	          <th width="20%">Request DateTime</th>
	          <th width="20%">Return DateTime</th>
	          <th width="12%">Status</th>
	        </thead>
	        <tbody>
	          @foreach($service_request as $row)
				@if($row->request_status_id == 2)
				<tr class="legend-approved">
				@elseif($row->request_status_id == 1)
				<tr class="legend-pending">
				@elseif($row->request_status_id == 5)
				<tr class="legend-closed">
				@elseif($row->request_status_id == 4)
				<tr class="legend-denied">
				@elseif($row->request_status_id == 3)
				<tr class="legend-cancelled">
				@endif
		            <td>
		            	@if($row->request_status_id == 2)
			            	<a href="{{ route('service_request_return', $row->request_id) }}" title="Return Requested Item"><i class="fa fa-refresh"></i></a>
			            	<a target="_blank" href="{{ route('service_request_receipt', $row->request_id) }}"> <i class="fa fa-print"></i></a>
		              	@else
		              		<a href="{{ route('service_request_items', $row->request_id) }}" title="Open Ticket"><i class="fa fa-tag"></i></a>
		              	@endif
		            	
		            	@can('admin-access')
			              	@if(in_array($row->request_status_id, [1]))
				            <a href="javascript:void(0)" onclick="confirmation('{{ route('service_request_deny', [$row->request_id]) }}', 'deny')" title="Deny Request?"><i class="fa fa-times"></i></a>
			            	@endif
		            	@endcan

			            @if(Auth::user()->request_id == $row->user_id && in_array($row->request_status_id, [1]))
			            	<a href="javascript:void(0)" onclick="confirmation('{{ route('service_request_cancel', [$row->request_id]) }}', 'cancel')" title="Cancel Request?"><i class="fa fa-ban"></i></a>
		            	@endif
		            </td>
		            <td>{{ $row->request_id }}</td>
		            <td>{{ $row->requester }}</td>
		            <td>{{ $row->request_datetime }}</td>
		            <td>{{ $row->return_datetime }}</td>
		            <td>{{ $row->request_status->name }}</td>
		          </tr>
	          @endforeach
	        </tbody>
      	</table>
        {!! $service_request->appends([ 'status'=>$status, 'search'=>$search ])->render() !!}
    </div>
</div>
<!-- Container Content -->
<script type="text/javascript">
$(function(){
	$('#status').val('<?php echo $status; ?>');
});
function confirmation(url, caption)
{
	if(confirm('Are you sure to ' + caption + ' this request?') == true)
	{
		window.location.replace(url);
		return true;
	}
}
</script>
<!-- Stops Container -->
@stop