@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
<div class="row" >
    <div class="medium-12 columns">
    	<h4><a href="{{ route('service_request') }}">Service Request</a> # <u>{{ $id }}</u></h4>
    </div>
</div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
<div class="row">
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-5 columns">Requester</div>
		    <div class="medium-7 columns">{{ $request->user->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Request Datetime</div>
		    <div class="medium-7 columns">{{ $request->request_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Return Datetime</div>
		    <div class="medium-7 columns">{{ $request->return_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Status</div>
		    <div class="medium-7 columns">{{ $request->request_status->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Date Created</div>
		    <div class="medium-7 columns">{{ $request->created_at }}</div>
		</div>
	</div>
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-2 columns">Remarks</div>
		    <div class="medium-10 columns">{{ $request->remarks }}</div>
		</div>
	</div>
</div>

<div class="row" >
    <div class="medium-12 columns">
      	<table class="hover">
	        <thead>
				<th width="10%"></th>
				<th width="65%">Item Name</th>
				<th width="10%">Quantity</th>
				<th width="15%">Status</th>
	        </thead>
	        <tbody>
	        	@foreach($rows as $row)
	        		<?php
	        		$item = App\Item::whereItemBrandId($row->item_brand_id)->whereItemCategoryId($row->item_category_id)->first();
	        		?>
			        	<tr>
				            <td>
				            	@if($request->request_status_id == 1)
				            		<a href="{{ route('service_request_assign_items', ['request_id'=>$row->request_id, 'brand_id'=>$row->item_brand_id, 'category_id'=>$row->item_category_id]) }}" title="Assign Item"><i class="fa fa-paperclip"></i></a>
				            		@if($row->request_brand_status_id == 1)
					            		@if(Auth::user()->id == $request->user_id)
						            		@if(in_array($request->request_status_id, [1, 2, 4]))
						              			<a href="javascript:void(0)" onclick="confirmation('{{ route('service_request_items_cancel', [$row->id]) }}', 'cancel')" title="Cancel item?"><i class="fa fa-ban"></i></a>
						            		@endif
						            	@endif
				            		@endif

					            	@can('admin-access')
						              	@if(in_array($row->request_brand_status_id, [1]))
							            <a href="javascript:void(0)" onclick="confirmation('{{ route('service_request_items_deny', [$row->id]) }}', 'deny')" title="Deny item?"><i class="fa fa-times"></i></a>
						            	@endif
					            	@endcan
				            	@endif
				            </td>
				            <td>{{ $item->brand->name . ' - ' . $item->category->name }}</td>
				            <td>{{ $row->quantity }}</td>
				            <td>{{ $row->request_item_status->name }}</td>
			        	</tr>
	        	@endforeach
	        </tbody>
      	</table>
    </div>
</div>
@if($request->request_status_id == 1)
<hr />
<div class="row">
	<div class="medium-6 columns">
		<form action="{{ route('service_request_approval', $request->id) }}" method="post">
		    <div class="row">
				<div class="medium-12 columns">
					<label for="remarks" class="text-left">Remarks</label>
					<textarea id="remarks" name="remarks" rows="10"></textarea>
				</div>
		    </div>
			<div class="row">
				<div class="medium-12 columns">
				<button class="button success" name="approved" value="2"><i class="fa fa-check"></i> Approved</button>
				<button class="button alert" name="denied" value="4"><i class="fa fa-times"></i> Deny</button>
					<a href="{{ route('service_request') }}" class="button secondary">Back</a>
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
	</div>
	<div class="medium-6 columns">&nbsp;</div>
</div>
@endif
<!-- Container Content -->
<script type="text/javascript">
function confirmation(url, caption)
{
	if(confirm('Are you sure to ' + caption + ' this item?') == true)
	{
		window.location.replace(url);
		return true;
	}
}
</script>
<!-- Stops Container -->
@stop