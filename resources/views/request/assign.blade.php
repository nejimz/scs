@extends('layouts.app')

@section('content')
<!-- Starts Container -->
        
  <!-- Container Title -->
<div class="row" >
    <div class="medium-12 columns">
    	<h4><a href="{{ route('service_request_items', $id) }}">Service Request</a> # <u>{{ $id }}</u></h4>
    </div>
</div>
  <!-- Container Title -->

  @include('layouts.message')

  <!-- Container Content -->
<div class="row">
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-5 columns">Requester</div>
		    <div class="medium-7 columns">{{ $request->user->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Request Datetime</div>
		    <div class="medium-7 columns">{{ $request->request_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Return Datetime</div>
		    <div class="medium-7 columns">{{ $request->return_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Status</div>
		    <div class="medium-7 columns">{{ $request_item->request_item_status->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns">Date Created</div>
		    <div class="medium-7 columns">{{ $request->created_at }}</div>
		</div>
	</div>
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-2 columns">Remarks</div>
		    <div class="medium-10 columns">{{ $request->remarks }}</div>
		</div>
	</div>
</div>

<form action="{{ route('service_request_assign_items_submit', ['request_id'=>$id, 'brand_id'=>$brand_id, 'category_id'=>$category_id]) }}" method="post">
	<div class="row" >
	    <div class="medium-12 columns">
	      	<table class="hover">
		        <thead>
					<th width="5%"><input type="checkbox" id="checkAll"></th>
					<th width="15%">ISBN</th>
					<th width="20%">Brand</th>
					<th width="20%">Category</th>
					<th width="40%">Item Name</th>
		        </thead>
		        <tbody>
		        	@foreach($rows as $row)

			        	<?php

			        	$condition_1 = App\ServiceRequestBorrowedItem::whereRequestId($request->id)->whereItemId($row->id)->count() != 0;
			        	$condition_2 = App\ServiceRequestBorrowedItem::whereItemId($row->id)->count() == 0;

                		$total_quantity = App\Item::where('item_category_id', $row->item_category_id)->where('item_brand_id', $row->item_brand_id)->count();

		                $items = App\ServiceRequestItem::leftJoin('request', 'request.id', '=', 'request_item.request_id')
		                ->where('request_brand_status_id', 2)
		                ->where('request_status_id', 2)
		                ->where('item_category_id', $row->item_category_id)
		                ->where('item_brand_id', $row->item_brand_id)
		                ->get();

		                foreach ($items as $item)
		                {
		                    if($item->request_datetime <= $from && $item->return_datetime >= $from)
		                    {
		                        $total_quantity -= $item->quantity;
		                    }
		                    elseif($item->request_datetime <= $to && $item->return_datetime >= $to)
		                    {
		                        $total_quantity -= $item->quantity;
		                    }
		                    elseif($from <= $item->request_datetime && $to >= $item->request_datetime)
		                    {
		                        $total_quantity -= $item->quantity;
		                    }
		                    elseif($from <= $item->return_datetime && $to >= $item->return_datetime)
		                    {
		                        $total_quantity -= $item->quantity;
		                    }
		                }

			        	?>

			        	@if($condition_1)
				        	<tr>
					            <td>
					            	@if($total_quantity > 0)
						            	@if($request_item->request_brand_status_id == 1)
						            		<input type="checkbox" name="item[{{ $row->id }}]" value="1">
						            	@elseif($request_item->request_brand_status_id == 2)
						            		@if(App\ServiceRequestBorrowedItem::whereRequestId($id)->whereItemId($row->id)->count() > 0)
						            			<i class="fa fa-check"></i>
						            		@endif
						            	@endif
					            	@endif
					            </td>
					            <td>{{ $row->isbn }}</td>
					            <td>{{ $row->brand->name }}</td>
					            <td>{{ $row->category->name }}</td>
					            <td>{{ $row->name }}</td>
				        	</tr>
				        @elseif($condition_2)
				        	@if($request_item->request_brand_status_id == 1)
				        	<tr>
					            <td>
					            	@if($total_quantity > 0)
						            	@if($request_item->request_brand_status_id == 1)
						            		<input type="checkbox" name="item[{{ $row->id }}]" value="1">
						            	@elseif($request_item->request_brand_status_id == 2)
						            		@if(App\ServiceRequestBorrowedItem::whereRequestId($id)->whereItemId($row->id)->count() > 0)
						            			<i class="fa fa-check"></i>
						            		@endif
						            	@endif
					            	@endif
					            </td>
					            <td>{{ $row->isbn }}</td>
					            <td>{{ $row->brand->name }}</td>
					            <td>{{ $row->category->name }}</td>
					            <td>{{ $row->name }}</td>
				        	</tr>
			        		@endif
			        	@endif

		        	@endforeach
		        </tbody>
	      	</table>
	    </div>
	</div>
	<div class="row">
		<div class="medium-12 columns">
			@if($request_item->request_brand_status_id == 1)
			<button type="submit" class="button success">Submit</button>
			@endif
	        {!! csrf_field() !!}
		</div>
	</div>
</form>
<!-- Container Content -->
<script type="text/javascript">
function confirmation(url, caption)
{
	if(confirm('Are you sure to ' + caption + ' this item?') == true)
	{
		window.location.replace(url);
		return true;
	}
}

$(function(){
	$('#checkAll').change(function(){
		$('input:checkbox').not(this).prop('checked', this.checked);
	});
});
</script>
<!-- Stops Container -->
@stop