@extends('layouts.app')

@section('content')
<!-- Starts Container -->

  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>

  <!-- Container Title -->
  <div class="row" >
    <div class="small-3 small columns">&nbsp;</div>
    <div class="small-3 small columns">
      <h4>New Service Request</h4>
    </div>
    <div class="small-6 small columns">&nbsp;</div>
  </div>
  <!-- Container Title -->

  <!-- Container Content -->
  <div class="row" >
    <div class="small-12 columns">
    	<form action="{{ $action }}" method="post">
	        <div class="row">
	        	<div class="small-6 columns">
	        		<div class="row">
						<div class="small-5 columns">
							<label for="request_datetime" class="text-right">Request Date Time</label>
						</div>
						<div class="small-7 columns">
							<input type="text" id="request_datetime" class="dtpicker" name="request_datetime" value="{{ $request_datetime }}">
						</div>
					</div>
					<div class="row">
						<div class="small-5 columns">
							<label for="return_datetime" class="text-right">Return Date Time</label>
						</div>
						<div class="small-7 columns">
							<input type="text" id="return_datetime" class="dtpicker" name="return_datetime" value="{{ $return_datetime }}">
						</div>
					</div>
					<div class="row">
						<div class="small-5 columns">
							<label for="remarks" class="text-right">Remarks</label>
						</div>
						<div class="small-7 columns">
							<textarea id="remarks" name="remarks" rows="10">{{ $remarks }}</textarea>
						</div>
					</div>
					<div class="row">
						<div class="small-5 columns">&nbsp;</div>
						<div class="small-7 columns">
							<button class="button primary"> Save</button>
							<a href="{{ route('service_request') }}" class="button secondary">Back</a>
						</div>
					</div>
	        	</div>
	        	<div class="small-6 columns">
	        		<div class="row">
	        			<div class="small-6 columns">
				            <div class="input-group">
								<input class="input-group-field" type="text" id="sr_search_item" name="sr_search_item" value="" autocomplete="off">
				            	<div class="input-group-button">
				                	<a href="javascript:void(0)" type="submit" class="button"><i class="fa fa-search"></i></a>
				            	</div>
				            </div>
	        			</div>
	        			<div class="small-6 columns">&nbsp;</div>
	        		</div>
	        		<div class="row">
	        			<div class="small-12 columns">
				          	<table id="sritems" width="100%">
				          		<thead>
				          			<th width="10%"></th>
				          			<th width="70%">Item</th>
				          			<th width="20%">Qty.</th>
				          		</thead>
				          		<tbody>
				          			@if(!is_null($item))
				          				@foreach($item as $key => $value)
				          					<tr id="sritem-{{ $key }}">
				          						<td><a href="javascript:void(0)" title="Remove" onclick="remove_sr_item('sritem-{{ $key }}')"><i class="fa fa-times"></i></a></td>
	            								<td>{{ $value['name'] }}<input type="hidden" name="item[{{ $key }}][name]" value="{{ $value['name'] }}"></td>
	            								<td><input type="number" name="item[{{ $key }}][quantity]" value="{{ $value['quantity'] }}" min="1" required /></td>
	            							</tr>
				          				@endforeach
				          			@endif
				          		</tbody>
				          	</table>
	        			</div>
	        		</div>
			    </div>
	        </div>
	        {!! csrf_field() !!}
    	</form>
    </div>
</div>
<!-- Container Content -->
<input type="hidden" id="sr_search_item_url" value="{{ route('item_search') }}">
<script type="text/javascript">
	var xhr_sr_search_item = null;

	$(function(){
	    $("#sr_search_item").autocomplete({
	        source: function (request, response) {
	        	var request_datetime = $('#request_datetime').val();
	        	var return_datetime = $('#return_datetime').val();

	        	if(request_datetime == '' || return_datetime == '')
	        	{
	        		alert('Request and Return Date Time is required!');
	        		return false;
	        	}

	        	if(xhr_sr_search_item != null)
	        	{
	        		return false;
	        	}

	            xhr_sr_search_item = $.ajax({
	                type : 'get',
	                url : $('#sr_search_item_url').val(),
	                data : 'search=' + request.term + '&request_datetime=' + request_datetime + '&return_datetime=' + return_datetime,
	                cache : true,
	                dataType : "json",
	                beforeSend: function(xhr){
	                    if (xhr_sr_search_item != null)
	                    {
	                        xhr_sr_search_item.abort();
	                    }
	                }
	            }).done(function(data){
	            	//console.log(data);
	                response($.map( data, function(value, key){
	                    return { label: value, value: key }
	                }));

	                xhr_sr_search_item = null;

	            }).fail(function(jqXHR, textStatus){
	                //console.log('Request failed: ' + textStatus);
	            });
	        }, 
	        minLength: 3,
	        autoFocus: true,
	        select: function(a, b){
	            var key = b.item.value;
	            var keyArr = key.split("-");
	            var quantity = keyArr[1];
	            var id = keyArr[0];
	            var name = b.item.label;
	            var row = 	'<tr id="sritem-' + id + '"><td><a href="javascript:void(0)" title="Remove" onclick="remove_sr_item(\'sritem-' + id + '\')"><i class="fa fa-times"></i></a></td>' + 
	            			'<td>' + name +'<input type="hidden" name="item[' + id + '][name]" value="' + name + '"></td>' + 
	            			'<td><input type="number" name="item[' + id + '][quantity]" value="1" min="1" max="' + keyArr[1] + '" required></td>' + '</tr>';

	            if(quantity > 0)
	            {
		            if($("#sritem-" + id).length == 0)
		            {
		            	$('table#sritems tbody').append(row);
		            }
	            }
		        
		        $('#sr_search_item').val('');

        		return false;
	        }
	    }).data("ui-autocomplete")._renderItem = function (ul, item) {
	        console.log(ul);
	        console.log(item);
	        var key = item.value;
	        var arr = key.split("-");

	        if(arr[1] == 0){
	            return $('<li class="ui-state-disabled">'+item.label+'</li>').appendTo(ul);
	        }else{
	            return $("<li>")
	            .append("<a>" + item.label + "</a>")
	            .appendTo(ul);
	        }
		};
	});

	function remove_sr_item(id){
		$('#' + id).remove();
	}
</script>
<!-- Stops Container -->
@stop