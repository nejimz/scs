@extends('layouts.app')

@section('content')
<!-- Starts Container -->

  <!-- Container Title -->
  <div class="row" >
    <div class="small-4 small-centered columns">
      <h4>Return Borrowed Item</h4>
    </div>
  </div>
  <!-- Container Title -->

<div class="row">
  <div class="medium-6 columns">
    <div class="row">
        <div class="medium-5 columns">Requester</div>
        <div class="medium-7 columns">{{ $sr->user->name }}</div>
    </div>
    <div class="row">
        <div class="medium-5 columns">Request Datetime</div>
        <div class="medium-7 columns">{{ $sr->request_datetime }}</div>
    </div>
    <div class="row">
        <div class="medium-5 columns">Return Datetime</div>
        <div class="medium-7 columns">{{ $sr->return_datetime }}</div>
    </div>
    <div class="row">
        <div class="medium-5 columns">Status</div>
        <div class="medium-7 columns">{{ $sr->request_status->name }}</div>
    </div>
    <div class="row">
        <div class="medium-5 columns">Date Created</div>
        <div class="medium-7 columns">{{ $sr->created_at }}</div>
    </div>
  </div>
  <div class="medium-6 columns">
    <div class="row">
        <div class="medium-2 columns">Remarks</div>
        <div class="medium-10 columns">{{ $sr->remarks }}</div>
    </div>
  </div>
</div>
<hr>
  <!-- Container Content -->
  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>
  <div class="row" >
    <div class="small-6 small-centered columns">
      <form action="{{ $action }}" method="post">

        <div class="row">
          <div class="small-2 columns">
            <label for="isbn" class="text-right">ISBN</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="isbn" name="isbn" value="{{ $row->item->isbn }}" readonly>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="name" class="text-right">Name</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="name" name="name" value="{{ $row->item->name }}" readonly>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="brand" class="text-right">Brand</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="brand" name="brand" value="{{ $row->item->brand->name }}" readonly>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="category" class="text-right">Category</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="category" name="category" value="{{ $row->item->category->name }}" readonly>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="request_return_status_id" class="text-right">Status</label>
          </div>
          <div class="small-10 columns">
            <select id="request_return_status_id" name="request_return_status_id">
              <option value=""></option>
              @foreach($return_status as $v)
              <option value="{{ $v->id }}">{{ $v->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="row">
          <div class="small-2 columns">
            <label for="remarks" class="text-right">Remarks</label>
          </div>
          <div class="small-10 columns">
            <textarea id="remarks" name="remarks" rows="10">{{ $remarks }}</textarea>
          </div>
        </div>

        <div class="row">
          <div class="small-2 columns">&nbsp;</div>
          <div class="small-10 columns">
            <button class="button primary"> Save</button>
            <a href="{{ route('service_request_return', $row->request_id) }}" class="button secondary">Back</a>
          </div>
        </div>
        <input type="hidden" name="request_id" id="request_id" value="{{ $row->request_id }}">
        <input type="hidden" name="request_borrowed_item_id" id="request_borrowed_item_id" value="{{ $row->id }}">
        <input type="hidden" name="request_item_id" id="request_borrowed_item_id" value="{{ $row->item_id }}">
        {!! csrf_field() !!}

      </form>
    </div>
  </div>
  <!-- Container Content -->
<script type="text/javascript">
$('#request_return_status_id').val('<?php echo $request_return_status_id; ?>');
</script>
<!-- Stops Container -->
@stop