@extends('layouts.app')

@section('content')
<!-- Starts Container -->
  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>
  <!-- Container Title -->
  <form action="{{ $action }}" method="post">
  <!-- Container Content -->
  <ul class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs id="deeplinked-tabs">
  <li class="tabs-title is-active"><a href="#personal" aria-selected="true">Personal Information</a></li>
  <li class="tabs-title"><a href="#account">Account Settings</a></li>
</ul>
  <div class="tabs-content" data-tabs-content="deeplinked-tabs" style="margin-bottom:20px;" >
    <div class="tabs-panel is-active" id="personal">
      <p>
        <div class="row">
          <div class="small-8 small-centered columns">
            <div class="row">
              <div class="small-6 columns text-right"><strong>School ID</strong></div>
              <div class="small-6 columns">{{ $school_id or 'N/A'}}</div>
            </div>
            <div class="row">
              <div class="small-6 columns text-right"><strong>First Name</strong></div>
              <div class="small-6 columns">{{ $first_name or 'N/A'}}</div>
            </div>
            <div class="row">
              <div class="small-6 columns text-right"><strong>Middle Name</strong></div>
              <div class="small-6 columns">{{ $middle_name or 'N/A'}}</div>
            </div>
            <div class="row">
              <div class="small-6 columns text-right"><strong>Last Name</strong></div>
              <div class="small-6 columns">{{ $last_name or 'N/A'}}</div>
            </div>
            <div class="row">
              <div class="small-6 columns text-right"><strong>Address</strong></div>
              <div class="small-6 columns">{{ $address or 'N/A'}}</div>
            </div>
            @if(isset(Auth::user()->profile))
            @if(Auth::user()->profile->position_id < 2)
            <div class="row">
              <div class="small-6 columns text-right"><strong>Grade</strong></div>
              <div class="small-6 columns">{{ $grade or ''}}</div>
            </div>
            <div class="row">
              <div class="small-6 columns text-right"><strong>Section</strong></div>
              <div class="small-6 columns">{{ $section or ''}}</div>
            </div>
            @else
            <div class="row">
              <div class="small-6 columns text-right"><strong>Education Background</strong></div>
              <div class="small-6 columns">{{ $educational_background or ''}}</div>
            </div>
            @endif
            @endif
          </div>
        </div>
      </p>
    </div>
    <div class="tabs-panel" id="account">
    <p>
  <div class="row" >
    <div class="small-6 small-centered columns">

        <div class="row">
          <div class="small-2 columns">
            <label for="name" class="text-right">Name</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="name" name="name" value="{{ $name }}">
          </div>
        </div>

        <div class="row">
          <div class="small-2 columns">
            <label for="email" class="text-right">Username</label>
          </div>
          <div class="small-10 columns">
            <input type="text" id="email" name="email" value="{{ $email }}">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">
            <label for="password_old" class="text-right">Old Password</label>
          </div>
          <div class="small-10 columns">
            <input type="password" id="password_old" name="password_old">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">
            <label for="email" class="text-right">New Password</label>
          </div>
          <div class="small-10 columns">
            <input type="password" id="password" name="password">
          </div>
        </div>
        <div class="row">
          <div class="small-2 columns">
            <label for="password_confirmation" class="text-right">Confirm Password</label>
          </div>
          <div class="small-10 columns">
            <input type="password" id="password_confirmation" name="password_confirmation">
          </div>
        </div>
 </p>
    </div>
  </div>
        <div class="row">
          <div class="small-4 small-centered columns">
            <button class="button primary"> Save</button>
          </div>
        </div>
        {!! csrf_field() !!}
      </form>
    </div>
  </div>
  <!-- Container Content -->
<script type="text/javascript">
</script>
<!-- Stops Container -->
@stop