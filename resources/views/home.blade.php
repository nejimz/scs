@extends('layouts.app')

@section('content')
<div id="charts-div">
    <!-- Annual Request Summary -->
    @include('layouts.message')
    <div class="row">
        <div class="small-4 columns">
            <div class="callout small" style="background: #337ab7; color: #fff;">
              <h3 style="padding:15px;"><i class="fa fa-users"></i> Users <span style="float:right">{{ App\User::get()->count() }}</span></h3>
            </div>
            <div class="callout small" style="background: #5cb85c; color: #fff;">
              <h3 style="padding:15px;"><i class="fa fa-file-text"></i> Requests <span style="float:right">{{ App\ServiceRequest::get()->count() }}</span></h3>
            </div>
            <div class="callout small" style="background: #5bc0de; color: #fff;">
              <h3 style="padding:15px;"><i class="fa fa-dropbox"></i> Items <span style="float:right">{{ App\Item::get()->count() }}</span></h3>
            </div>
        </div>
        <div class="small-8 columns">
            <div id="annual_requests"></div>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <div id="request_item_most"></div>
        </div>
        <div class="small-6 columns">
            <div id="request_user_most"></div>
        </div>
    </div>
<!--     <div class="row">
        <div class="small-6 columns">
            <label>Most Request User (Top 10)</label>
        </div>
        <div class="small-6 columns">
            <label>Least Request User (Top 10)</label>
        </div>
    </div> -->
    <div class="row">
        <div class="small-6 columns">
            <div id="borrowed_item_most"></div>
        </div>
        <div class="small-6 columns">
            <div id="borrowed_item_most"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    Highcharts.chart('annual_requests', {
        chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie' },
        title: { text: 'Annual Request Summary' },
        tooltip: { mpointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{ name: 'Brands', colorByPoint: true, data: {!! $annual_requests->toJson() !!} }]
    });

    Highcharts.chart('request_item_most', {
        chart: { type: 'column' },
        title: { text: 'Most Request Item (Top 10)' },
        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            // your statements here
                            window.location.href = "{{ route('dashboard') }}/dss/" + this.url + "/item";
                        }
                    }
                }
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Requests'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>{point.y} requests</b>'
        },
        series: [{
            name: 'Request',
            data: {!! $most_requested->toJson() !!}
        }]
    });

    Highcharts.chart('request_user_most', {
        chart: { type: 'column' },
        title: { text: 'Most Request User (Least 10)' },
        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            // your statements here
                            window.location.href = "{{ route('dashboard') }}/dss/" + this.url + "/user";
                        }
                    }
                }
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Requests'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>{point.y} requests</b>'
        },
        series: [{
            name: 'Request',
            data: {!! $most_user->toJson() !!}
        }]
    });
</script>
<style type="text/css">
    #charts-div label{
        font-weight: bold;
    }
    .highcharts-credits{
        display:none;
    }
</style>
@endsection
