@extends('layouts.app')

@section('content')
	<p class="text-right">
		<a href="{{ route('home') }}" class="button">Return</a>
	</p>
	<table>
		<thead>
			<th>Requester</th>
			<th>Brand</th>
			<th>Request Date</th>
			<th>Quantity</th>
			<th>Return Date</th>
			<th>Requested On</th>
		</thead>	
		<tbody>
			@foreach($items as $item)
				<tr>
					<td>{{ $item->user->name }}</td>
					<td>{{ $item->name }}</td>
					<td>{{ $item->request_datetime }}</td>
					<td>{{ $item->quantity }}</td>
					<td>{{ $item->return_datetime }}</td>
					<td>{{ $item->created_at }}</td>
				</tr>
			@endforeach
		</tbody>	
	</table>
@endsection
