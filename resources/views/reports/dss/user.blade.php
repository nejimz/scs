@extends('layouts.app')

@section('content')
	<p class="text-right">
		<a href="{{ route('home') }}" class="button">Return</a>
	</p>
	<table>
		<thead>
			<th>Requester</th>
			<th>Request Date</th>
			<th>Request Details</th>
			<th>Return Date</th>
			<th>Requested On</th>
		</thead>	
		<tbody>
			@foreach($users->requests as $item)
				<tr>
					<td>{{ $item->user->name }}</td>
					<td>{{ $item->request_datetime }}</td>
					<td>
						<ul>
							@foreach($item->request_items as $req)
								<li>{{ $req->brand->name }} <span style="float:right;">({{ $req->quantity }})</span></li>
							@endforeach
						</ul>
					</td>
					<td>{{ $item->return_datetime }}</td>
					<td>{{ $item->created_at }}</td>
				</tr>
			@endforeach
		</tbody>	
	</table>
@endsection
