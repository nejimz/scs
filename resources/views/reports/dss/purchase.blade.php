@extends('layouts.app')

@section('content')
	<p class="text-right">
		<a href="{{ route('home') }}" class="button">Return</a>
		<table>
			<thead>
				<tr>
					<th>Item</th>
					<th>Supply (# of Items)</th>
					<th>Defective Count</th>
					<th>Demand (# of Requests)</th>
					<th>Demand Supply Ratio</th>
				</tr>
			</thead>
			<tbody>
				@foreach($high_demand_items as $item)
				<tr>
					<td>{{ App\ItemBrand::find($item['brand_id'])->name }}</td>
					@if(is_null(App\ItemBrand::find($item['brand_id'])->items))
						<td>0</td>
					@else
						<td>{{ count(App\ItemBrand::find($item['brand_id'])->items) }}</td>
					@endif
					@if(is_null(App\ItemBrand::find($item['brand_id'])->items))
						<td>0</td>
					@else
						<td>{{ App\ItemBrand::find($item['brand_id'])->items->where('status',3)->count() }}</td>
					@endif
					<td>{{ App\ItemBrand::find($item['brand_id'])->requests->count() }}</td>
					@if(is_null(App\ItemBrand::find($item['brand_id'])->items))
						<td>0</td>
					@else
						<td>{{ number_format(App\Item::division(App\ItemBrand::find($item['brand_id'])->items->count(), App\ItemBrand::find($item['brand_id'])->requests->count()), 2, '.', '') }}</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</p>
@endsection
