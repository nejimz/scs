@extends('layouts.app')

@section('content')
<!-- Starts Container -->    

  <div class="row" >
    <div class="small-6 small-centered columns"> 
      @include('layouts.message')
    </div>
  </div>

  <!-- Container Title -->
  <div class="row" >
    <div class="small-4 small-centered columns">
      <h4>Reports</h4>
    </div>
  </div>

  <!-- Container Title -->
  <div class="row" >
    <div class="small-6 small-centered columns">
      <form action="{{ route('report_generate') }}" target="_blank" method="post">
      <div class="row">
        <div class="small-2 columns">
          <label for="item_brand_id" class="text-right">Reports</label>
        </div>
        <div class="small-10 columns">
          <select id="report_type" name="report_type">
            <option value=""></option>
            <option value="items">Item</option>
            <option value="requests">Request</option>
            <option value="users">User</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="small-2 columns">
          <label for="return_datetime" class="text-right">Date Start</label>
        </div>
        <div class="small-10 columns">
          <input type="text" id="report_date_start" data-date-format="yyyy-mm-dd" name="report_date_start" value="{{ date('Y-m-d') }}">
        </div>
      </div>
      <div class="row">
        <div class="small-2 columns">
          <label for="return_datetime" class="text-right">Date End</label>
        </div>
        <div class="small-10 columns">
          <input type="text" id="report_date_end" data-date-format="yyyy-mm-dd" name="report_date_end" value="{{ date('Y-m-d') }}">
        </div>
      </div>
      <div class="row">
        <div class="small-2 columns">&nbsp;</div>
        <div class="small-10 columns">
            <button class="button primary"> Generate</button>
        </div>
        </div>
    </div>
      {!! csrf_field() !!}
    </form>
  </div>
      <!-- Container Content -->        
<!-- Stops Container -->
<script type="text/javascript">
  $('form').submit(function(e){
      if($('select option:selected').val() === '')
      {
        alert("Please select a report type.");
        e.preventDefault();
      }
  });
</script>
@stop