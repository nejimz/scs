@extends('layouts.report')

@section('content')
<!-- Starts Container -->    
<div class="row">
  <div class="small-8 small-centered columns">
  	@if(date("M d, Y", strtotime($report_date_start))==date("M d, Y", strtotime($report_date_end)))
  	<h3><center>{{ title_case(str_singular($report_title)) }} Report for {{ date("F d, Y", strtotime($report_date_start)) }}</center></h3>
  	@else
    <h3><center>{{ title_case(str_singular($report_title)) }} Report from {{ date("M d, Y", strtotime($report_date_start)) }} to {{ date("M d, Y", strtotime($report_date_end)) }}</center></h3>
    @endif
    <p><center>{{ config('app.name') }}</center></p>
  </div>
</div>     
@include('reports.partials.'.str_singular($report_title))

<!-- Stops Container -->
@stop