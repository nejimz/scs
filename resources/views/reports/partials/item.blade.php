<div class="row">
  <div class="small-12 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="4">Item Brand</th></tr>
        <tr>
          <th>Brand</th>
          <th>Available</th>
          <th>Borrowed</th>
          <th>Defective</th>
        </tr>
      </thead>
      <tbody>
        @if($brands->count() > 0)
          @foreach($brands as $brand)
              <tr>
                <td>{{ $brand->brand->name }}</td>
                <td>{{ $brand->available_count }} </td>    
                <td>{{ $brand->borrowed_count }} </td>   
                <td>{{ $brand->defective_count }} </td>   
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="small-12 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="4">Item Category</th></tr>
        <tr>
          <th>Category</th>
          <th>Available</th>
          <th>Borrowed</th>
          <th>Defective</th>
        </tr>
      </thead>
      <tbody>
        @if($brands->count() > 0)
          @foreach($categories as $category)
              <tr>
                <td>{{ $category->category->name }}</td>
                <td>{{ $category->available_count }} </td>    
                <td>{{ $category->borrowed_count }} </td>   
                <td>{{ $category->defective_count }} </td>   
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>     
<div class="row">
  <div class="small-6 columns">
    
  </div>
</div>  
     <h5>Item Manifest</h5>
     <table>
       <thead>
         <tr>
          <th>Date Added</th>
          <th>ISBN</th>
          <th>Name</th>
          <th>Brand</th>
          <th>Category</th>
         </tr>
       </thead>
       <tbody>
         @foreach($items as $item)
          <tr>
            <td><small>{{ $item->created_at }}</small></td>
            <td>{{ strtoupper($item->isbn) }}</td>
            <td>{{ strtoupper($item->name) }}</td>
            <td>{{ strtoupper($item->brand->name) }}</td>
            <td>{{ strtoupper($item->category->name) }}</td>
          </tr>
         @endforeach
       </tbody>
     </table>