<div class="row">
  <div class="small-12 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="4">User Roles</th></tr>
        <tr>
          <th>Role</th>
          <th>Active</th>
          <th>Inactive</th>
        </tr>
      </thead>
      <tbody>
        @if($roles->count() > 0)
          @foreach($roles as $role)
              <tr>
                <td>{{ $role->role_alias }}</td>
                <td>{{ $role->active_count }}</td>
                <td>{{ $role->inactive_count }} </td>    
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>   
<div class="row">
  <div class="small-6 columns">
    
  </div>
</div>  
     <h5>User Manifest</h5>
     <table>
       <thead>
         <tr>
          <th>Name</th>
          <th>Username</th>
          <th>Role</th>
          <th>Last Login</th>
         </tr>
       </thead>
       <tbody>
         @foreach($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>{{ $user->updated_at }}</td>
          </tr>
         @endforeach
       </tbody>
     </table>