<div class="row">
  <div class="small-6 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="4">10 Most Requested Item Summary</th></tr>
        <tr>
          <th>Brand</th>
          <th>Requests</th>
        </tr>
      </thead>
      <tbody>
        @if($most_requested->count() > 0)
          @foreach($most_requested as $requested)
              <tr>
                <td>{{ $requested->name }}</td>
                <td>{{ $requested->y }} </td>    
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
  <div class="small-6 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="4">10 Users With Most Summary</th></tr>
        <tr>
          <th>Name</th>
          <th>Requests</th>
        </tr>
      </thead>
      <tbody>
        @if($most_user->count() > 0)
          @foreach($most_user as $user)
              <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->y }} </td>    
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="row">
  <div class="small-12 columns">
    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr><th colspan="10">Request Status Summary</th></tr>
      </thead>
      <tbody>
        @if($request_summary->count() > 0)
          @foreach($request_summary as $request_status)
              <tr>
                <th style="width: 10%">Pending</th>
                <td style="width: 10%">{{ $request_status->pending_count }}</td>
                <th style="width: 10%">Approved</th>
                <td style="width: 10%">{{ $request_status->approved_count }}</td>
                <th style="width: 10%">Cancelled</th>
                <td style="width: 10%">{{ $request_status->cancelled_count }}</td>
                <th style="width: 10%">Denied</th>
                <td style="width: 10%">{{ $request_status->denied_count }}</td>
                <th style="width: 10%">Closed</th>
                <td style="width: 10%">{{ $request_status->closed_count }}</td>
              </tr>   
          @endforeach
        @else
          <tr><td colspan="4"><center>No data</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<h5>Requests Manifest</h5>
 <table>
   <thead>
     <tr>
      <th>Requester</th>
      <th>Items</th>
      <th>Requested At</th>
      <th>Status</th>
     </tr>
   </thead>
   <tbody>

     @foreach($requests_lists as $req)
      <tr>
        <td style="width: 35%">{{ $req->user->name }}</td>
        <td>
          <ul>
            @if(App\ServiceRequestBorrowedItem::whereRequestItemId($req->id)->count()>0)
            @foreach(App\ServiceRequestBorrowedItem::whereRequestItemId($req->id)->get() as $borrowed_item)
              <li>{{ $borrowed_item->item->name }}</li>
            @endforeach
            @else
              <li>NO ITEM ASSIGNED</li>
            @endif
          </ul>
        </td>
        <td style="width: 15%">{{ $req->created_at }}</td>
        <td style="width: 10%">{{ $req->request_status->name }}</td>
      </tr>
     @endforeach
   </tbody>
 </table>