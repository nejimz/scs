@extends('layouts.report')

@section('content')
<!-- Starts Container -->    
<div class="row" >
    <div class="medium-12 columns">
    	<h5><center><strong>Service Request Reciept</strong></center></h5>
    </div>
</div>
<div class="row" >
    <div class="medium-6 columns">
    	<p><strong>Service Request</strong> #{{ str_pad($id, 6, '0', STR_PAD_LEFT) }}</p>
    </div>
    <div class="medium-6 columns">
    	<p class="text-right"><strong>Print Date</strong> {{ date('Y-m-d H:i:s') }}</p>
    </div>
</div>
<div class="row" style="margin-bottom:20px;">
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-5 columns"><strong>Requester</strong></div>
		    <div class="medium-7 columns">{{ $sr->user->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns"><strong>Request Datetime</strong></div>
		    <div class="medium-7 columns">{{ $sr->request_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns"><strong>Return Datetime</strong></div>
		    <div class="medium-7 columns">{{ $sr->return_datetime }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns"><strong>Status</strong></div>
		    <div class="medium-7 columns">{{ $sr->request_status->name }}</div>
		</div>
		<div class="row">
		    <div class="medium-5 columns"><strong>Date Created</strong></div>
		    <div class="medium-7 columns">{{ $sr->created_at }}</div>
		</div>
	</div>
	<div class="medium-6 columns">
		<div class="row">
		    <div class="medium-2 columns"><strong>Remarks</strong></div>
		    <div class="medium-10 columns">{{ $sr->remarks }}</div>
		</div>
	</div>
</div>

<div class="row" >
    <div class="medium-12 columns">
      	<table class="hover" border="1" cellpadding="0" cellspacing="0">
	        <thead>
				<th width="35%">Brand</th>
				<th width="35%">Item/s</th>
				<th width="10%">Quantity</th>
				<th width="15%">Status</th>
	        </thead>
	        <tbody>
	        	@foreach($sr_rows as $row)
	        		<tr>
	        			<td>{{ $row->brand->name }}</td>
	        			<td>
	        				<ul style="list-style: none;">
	        				@foreach(App\ServiceRequestBorrowedItem::whereRequestItemId($row->id)->get() as $v)
	        					<li><p>
	        						<span><img src='data:image/png;base64,{{ DNS2D::getBarcodePNG(route("service_request_return_item", $v->id), "QRCODE",3,3) }}' alt="barcode"/></span>
	        						@if(App\ServiceRequestReturnItem::where('request_borrowed_item_id', $v->id)->count() == 0)
	        							{{ $v->item->name }}
	        						@else
	        							{{ $v->item->name }}
	        						@endif
	        					</p></li>
	        				@endforeach
	        				</ol>
	        			</td>
	        			<td>{{ $row->quantity }}</td>
	        			<td>{{ $row->request_item_status->name }}</td>
	        		</tr>
	        	@endforeach
	        </tbody>
      	</table>
    </div>
</div>

<!-- Stops Container -->
@stop