<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use App\LogUsers;
use App\User;
use Auth;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // if(Auth::user()->role=="")
        // {
        //     User::whereId(Auth::id())->update(['role' => "basic", 'active' => 1]);
        // }
        // 
        if(Auth::user()->active==0)
        {
            Auth::logout();
            $bag = new MessageBag();
            $bag->add('errors', 'Your account is banned or disabled.');
            $data = compact('bag');
            return redirect()->back()->with('errors', session()->get('errors', new ViewErrorBag)->put('default', $bag));
        }

        $login = new LogUsers();
        $login->user_id = Auth::id();
        $login->lilo = 1;
        $login->logged_on = date('Y-m-d H:i:s');
        $login->save();
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //
    }
}
