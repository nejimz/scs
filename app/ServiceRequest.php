<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use App\User;
use App\ServiceRequestStatus;

class ServiceRequest extends Model
{
    public $table = 'request';
    public $fillable = ['request_status_id', 'request_datetime', 'return_datetime', 'remarks', 'user_id', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function request_status()
    {
        return $this->hasOne('App\ServiceRequestStatus', 'id', 'request_status_id');
    }

    public function request_item()
    {
        return $this->belongsTo('App\ServiceRequestItem', 'id', 'request_id');
    }

    public function request_items()
    {
        return $this->hasMany('App\ServiceRequestItem', 'request_id', 'id');
    }

    public function request_remarks()
    {
        return $this->hasOne('App\ServiceRequestRemarks', 'id', 'request_id');
    }

    public function setRequestDatetimeAttribute($value)
    {
    	$this->attributes['request_datetime'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setReturnDatetimeAttribute($value)
    {
    	$this->attributes['return_datetime'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function getRequesterAttribute($value)
    {
    	$row = User::whereId($this->user_id)->first();
    	return $row->name;
    }

    /*public function getRequestStatusAttribute($value)
    {
    	$row = ServiceRequestStatus::whereId($this->request_status_id)->first();
    	return $row->name;
    }*/
    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
   public function getRequestDatetimeAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
      public function getReturnDatetimeAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
}
