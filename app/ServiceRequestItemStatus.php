<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestItemStatus extends Model
{
    public $table = 'request_item_status';
    public $fillable = ['name'];
    public $timestamps = false;

    public function service_request()
    {
        return $this->belongsTo('App\ServiceItemRequest', 'id', 'request_brand_status_id');
    }
}
