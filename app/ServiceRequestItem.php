<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestItem extends Model
{
    public $table = 'request_item';
    public $fillable = ['request_id', 'item_brand_id', 'item_category_id', 'quantity', 'request_brand_status_id'];
    public $timestamps = false;
    
    public function brand()
    {
        return $this->hasOne('App\ItemBrand', 'id', 'item_brand_id');
    }
    
    public function item_category()
    {
        return $this->hasOne('App\ItemCategory', 'id', 'item_id');
    }

    public function request()
    {
        return $this->hasOne('App\ServiceRequest', 'id', 'request_id');
    }
    
    public function request_item_status()
    {
        return $this->hasOne('App\ServiceRequestItemStatus', 'id', 'request_brand_status_id');
    }
    
    public function item_borrowed()
    {
        return $this->belongsTo('App\ServiceRequestBorrowedItem', 'id', 'request_item_id');
    }

    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
}
