<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestReturnItem extends Model
{
    public $table = 'request_return_item';
    public $fillable = ['request_id', 'request_borrowed_item_id', 'request_return_status_id', 'user_id', 'remarks'];
    public $timestamps = true;

    public function request_return_item_status()
    {
        return $this->belongsTo('App\ServiceRequestReturnItem', 'id', 'request_return_status_id');
    }

    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
}
