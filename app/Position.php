<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public $table = 'positions';
    public $fillable = ['name'];
    public $timestamps = false;
}
