<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
    public $table = 'item_status';
    public $fillable = ['name'];
    public $timestamps = false;

    public function item()
    {
        return $this->belongsTo('App\Item', 'id', 'status');
    }
}
