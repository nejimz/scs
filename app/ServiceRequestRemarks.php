<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestRemarks extends Model
{
	public $table = 'request_remarks';
    public $fillable = ['request_id', 'user_id', 'remarks', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function service_request()
    {
        return $this->belongsTo('App\ServiceRequest', 'id', 'request_id');
    }
}
