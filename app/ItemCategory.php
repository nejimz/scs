<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    public $table = 'item_category';
    public $fillable = ['name', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function item()
    {
        return $this->belongsTo('App\Item', 'id', 'item_category_id');
    }

    public function service_request_item()
    {
        return $this->belongsTo('App\ServiceRequestItem', 'id', 'item_id');
    }
}
