<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestStatus extends Model
{
	public $table = 'request_status';
    public $fillable = ['name'];
    public $timestamps = false;

    public function service_request()
    {
        return $this->belongsTo('App\ServiceRequest', 'id', 'request_status_id');
    }
}
