<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Event::listen('auth.*', function($event){ });

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);

// Registration Routes...
$this->get('register', 'Auth\AuthController@showRegistrationForm');
$this->post('register', 'Auth\AuthController@register');

//https://mattstauffer.co/blog/the-auth-scaffold-in-laravel-5-2
Route::get('/', ['middleware' => 'auth', 'as' => 'dashboard', 'uses' => 'DashboardController@index']);

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/profile', ['middleware' => 'auth', 'as' => 'user_profile', 'uses' => 'UserController@profile']);

Route::post('/profile/{user_id}/update', ['middleware' => 'auth', 'as' => 'user_profile_update', 'uses' => 'UserController@update_profile']);

// Password Reset ejZ5yr
Route::get('/reset_user_password', ['as' => 'reset_user_password', 'uses' => 'UserController@reset_user_password']);
Route::post('/reset_user_confirm', ['as' => 'reset_user_confirm', 'uses' => 'UserController@reset_user_confirm']);

Route::group(['prefix' => 'api'], function () {
	Route::group(['prefix' => 'notification'], function () {
		Route::get('request', ['as' => 'request_notification', 'uses' => 'ServiceRequestController@notification']);
	});
	Route::group(['prefix' => 'search'], function () {
		//Route::get('category', ['as' => 'item_category_search', 'uses' => 'ItemCategoryController@search']);
		Route::get('item', ['as' => 'item_search', 'uses' => 'ItemController@search']);
	});
});

Route::group(['prefix' => 'portal', 'middleware' => 'auth'], function () {

	Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
	
	Route::group(['prefix' => 'user', 'middleware' => 'admin'], function () {
		Route::get('/', ['as' => 'users', 'uses' => 'UserController@index']);
		Route::get('create', ['as' => 'user_create', 'uses' => 'UserController@create']);
		Route::post('store', ['as' => 'user_store', 'uses' => 'UserController@store']);
		Route::get('{user_id}/edit', ['as' => 'user_edit', 'uses' => 'UserController@edit']);
		Route::post('{user_id}/update', ['as' => 'user_update', 'uses' => 'UserController@update']);

		Route::get('{user_id}/penalties', ['as' => 'user_penalties', 'uses' => 'UserController@penalties']);
	});

	Route::group(['prefix' => 'reports', 'middleware' => 'admin'], function () {
		Route::get('/', ['as' => 'reports', 'uses' => 'ReportController@index']);
		Route::post('generate', ['as' => 'report_generate', 'uses' => 'ReportController@generate']);
	});

	Route::group(['prefix' => 'dss', 'middleware' => 'admin'], function() {
		Route::get('/', ['as' => 'puchase_dss', 'uses' => 'HomeController@purchasedss']);
		Route::get('{id}/user', ['as' => 'user_dss', 'uses' => 'HomeController@userdss']);
		Route::get('{id}/item', ['as' => 'item_dss', 'uses' => 'HomeController@itemdss']);
	});

	Route::group(['prefix' => 'item', 'middleware' => 'admin'], function () {
		Route::get('/', ['as' => 'item', 'uses' => 'ItemController@index']);
		Route::get('create', ['as' => 'item_create', 'uses' => 'ItemController@create']);
		Route::post('store', ['as' => 'item_store', 'uses' => 'ItemController@store']);
		Route::get('{item_id}/edit', ['as' => 'item_edit', 'uses' => 'ItemController@edit']);
		Route::post('{item_id}/update', ['as' => 'item_update', 'uses' => 'ItemController@update']);
		Route::get('{item_id}/qrprint', ['as' => 'item_qrprint', 'uses' => 'ItemController@qrprint']);

		Route::group(['prefix' => 'category'], function () {
			Route::get('/', ['as' => 'item_category', 'uses' => 'ItemCategoryController@index']);
			Route::get('create', ['as' => 'item_category_create', 'uses' => 'ItemCategoryController@create']);
			Route::post('store', ['as' => 'item_category_store', 'uses' => 'ItemCategoryController@store']);
			Route::get('{item_category_id}/edit', ['as' => 'item_category_edit', 'uses' => 'ItemCategoryController@edit']);
			Route::post('{item_category_id}/update', ['as' => 'item_category_update', 'uses' => 'ItemCategoryController@update']);
		});

		Route::group(['prefix' => 'brand'], function () {
			Route::get('/', ['as' => 'item_brand', 'uses' => 'ItemBrandController@index']);
			Route::get('create', ['as' => 'item_brand_create', 'uses' => 'ItemBrandController@create']);
			Route::post('store', ['as' => 'item_brand_store', 'uses' => 'ItemBrandController@store']);
			Route::get('{item_brand_id}/edit', ['as' => 'item_brand_edit', 'uses' => 'ItemBrandController@edit']);
			Route::post('{item_brand_id}/update', ['as' => 'item_brand_update', 'uses' => 'ItemBrandController@update']);
		});
	});

	Route::group(['prefix' => 'service-request'], function () {
		Route::get('/', ['as' => 'service_request', 'uses' => 'ServiceRequestController@index']);
		Route::get('{id}/deny', ['as' => 'service_request_deny', 'uses' => 'ServiceRequestController@request_deny']);
		Route::get('{id}/cancel', ['as' => 'service_request_cancel', 'uses' => 'ServiceRequestController@request_cancel']);

		Route::post('{id}/approval', ['as' => 'service_request_approval', 'uses' => 'ServiceRequestController@request_approval']);

		Route::get('{id}/return', ['as' => 'service_request_return', 'uses' => 'ServiceRequestController@request_return']);

		Route::get('{id}/receipt', ['as' => 'service_request_receipt', 'uses' => 'ServiceRequestController@request_receipt']);

		Route::get('{id}/closed', ['as' => 'service_request_closed', 'uses' => 'ServiceRequestController@request_closed']);

		Route::group(['prefix' => 'item'], function () {
			Route::get('{id}', ['as' => 'service_request_items', 'uses' => 'ServiceRequestController@request']);
			Route::get('{id}/deny', ['as' => 'service_request_items_deny', 'uses' => 'ServiceRequestController@item_deny']);
			Route::get('{id}/cancel', ['as' => 'service_request_items_cancel', 'uses' => 'ServiceRequestController@item_cancel']);
			Route::get('{id}/approved', ['as' => 'service_request_items_approved', 'uses' => 'ServiceRequestController@item_approved']);

			Route::get('{request_id}/{brand_id}/{category_id}/assign', ['as' => 'service_request_assign_items', 'uses' => 'ServiceRequestController@assign_item_request']);
			Route::post('{request_id}/{brand_id}/{category_id}/assign-submit', ['as' => 'service_request_assign_items_submit', 'uses' => 'ServiceRequestController@assign_item_request_submit']);

			Route::get('{request_borrowed_item_id}/return', ['as' => 'service_request_return_item', 'uses' => 'ServiceRequestController@return_item_request']);
			Route::post('{request_borrowed_item_id}/return-submit', ['as' => 'service_request_return_item_submit', 'uses' => 'ServiceRequestController@return_item_request_submit']);

			Route::get('{request_borrowed_item_id}/return-closed', ['as' => 'service_request_return_item_closed', 'uses' => 'ServiceRequestController@return_item_request_closed']);
			Route::post('{request_borrowed_item_id}/return-closed-submit', ['as' => 'service_request_return_item_closed_submit', 'uses' => 'ServiceRequestController@return_item_request_closed_submit']);
		});

		Route::get('create', ['as' => 'service_request_create', 'uses' => 'ServiceRequestController@create']);
		Route::post('store', ['as' => 'service_request_store', 'uses' => 'ServiceRequestController@store']);
		#Route::get('{user_id}/edit', ['as' => 'user_edit', 'uses' => 'ServiceRequestController@edit']);
		#Route::post('{user_id}/update', ['as' => 'user_update', 'uses' => 'ServiceRequestController@update']);
	});
});	

