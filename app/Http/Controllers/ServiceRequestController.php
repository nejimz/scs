<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestStatus;
use App\ServiceRequestRemarks;
use App\ServiceRequestBorrowedItem;
use App\ServiceRequestReturnItem;
use App\ServiceRequestReturnItemStatus;

use Validation;
use Session;
use \Carbon\Carbon;
use Auth;

class ServiceRequestController extends Controller
{
    public function index(Request $request)
    {
        $page_number = 30;
        $status = '';
        $search = '';
        $legend = ServiceRequestStatus::orderBy('sort', 'ASC')->get();

        $query = ServiceRequest::with('user')
        ->select('request.id AS request_id', 'request_status_id', 'request_datetime', 'return_datetime', 'remarks', 'user_id')
        ->leftJoin('request_status', 'request.request_status_id', '=', 'request_status.id');

        if($request->has('status'))
        {
            $status = trim($request->status);
            $query->whereRequestStatusId($status);
        }

        if(Auth::user()->role != 'admin')
        {
            $query->whereUserId(Auth::user()->id);
        }

        if($request->has('search'))
        {
            $search = trim($request->search);
            $query->where('id', 'LIKE', "%$search%");
            /*$query->where(function($inQuery) use ($search){
                $inQuery->where('id', 'LIKE', "%$search%");
            });*/
        }

        #$service_request = $query->orderBy('request_datetime', 'ASC')->toSql();dd($service_request);
        $service_request = $query->orderBy('sort', 'ASC')->orderBy('request_datetime', 'DESC')->paginate($page_number);

        $data = compact('status', 'search', 'legend', 'service_request');

        return view('request.index', $data);
    }

    public function request($id)
    {
        $request = ServiceRequest::whereId($id)->first();
        $rows = ServiceRequestItem::whereRequestId($id)->get();

        $data = compact('id', 'request', 'rows');

        return view('request.request', $data);
    }

    public function create()
    {
        $action = route('service_request_store');
        $request_datetime = old('request_datetime');
        $return_datetime = old('return_datetime');
        $remarks = old('remarks');
        $item = old('item');
        #dd($item);

        $data = compact('action', 'request_datetime', 'return_datetime', 'remarks', 'item');

        return view('request.form', $data);
    }

    public function store(Request $request)
    {
        $input = $request->only('request_datetime', 'return_datetime', 'remarks');
        $input['request_status_id'] = 1;
        $input['user_id'] = Auth::user()->id;

        $this->validate(
            $request,
            [
                'request_datetime' => 'required',
                'return_datetime' => 'required',
                'remarks' => 'required',
                'item' => 'required'
            ],
            [
                'request_datetime.required'=>'Request Date Time is required!',
                'return_datetime.required'=>'Return Date Time is required!',
                'remarks.required'=>'Category is required!',
                'item.required'=>'Item is required!'
            ]
        );
        $request_datetime = date('Y-m-d H:i:s', strtotime($request->request_datetime));
        $return_datetime = date('Y-m-d H:i:s', strtotime($request->return_datetime));

        if ($request_datetime >= $return_datetime)
        {
            return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Request and Return Date Time is invalid!');
        }

        #$service_request = ServiceRequest::whereRequestStatusId(1)->orderBy('request_datetime', 'ASC')->get();
        /*foreach($service_request as $row)
        {
            #return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item (' . $row->request_item->item->name . ') has already been scheduled!');
            $from = date('Y-m-d H:i:s', strtotime($request->request_datetime));
            $to = date('Y-m-d H:i:s', strtotime($request->return_datetime));

            echo $row->request_datetime . ' >= ' . $from . ' && ' . $row->return_datetime . ' <= ' . $from . '<br />';
            echo $row->request_datetime . ' >= ' . $to . ' && ' . $row->return_datetime . ' <= ' . $to . '<br /><br />';

            if($row->request_datetime <= $from && $row->return_datetime >= $from)
            {
                return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
            }
            elseif($row->request_datetime <= $to && $row->return_datetime >= $to)
            {
                return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
            }
            elseif($from <= $row->request_datetime && $to >= $row->request_datetime)
            {
                return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
            }
            elseif($from <= $row->return_datetime && $to >= $row->return_datetime)
            {
                return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
            }
        }*/

        $sr = ServiceRequest::create($input);

        foreach($request->item as $key => $value)
        {
            $row = Item::whereId($key)->first();

            ServiceRequestItem::create([
                'request_id'=>$sr->id, 
                'item_brand_id'=>$row->item_brand_id, 
                'item_category_id'=>$row->item_category_id, 
                'quantity'=>$value['quantity'], 
                'request_brand_status_id'=>1
            ]);
        }

        return redirect()->route('service_request_create')->with('success', 'Service Request successfuly sent!');
    }

    public function request_approval(Request $request, $id)
    {
        $rows = ServiceRequestItem::whereRequestId($id)->get();
        foreach($rows as  $row)
        {
            if($row->request_brand_status_id == 1)
            {
                return redirect()->route('service_request_items', $id)->withInput()->with('custom_error', $row->brand->name . ' is still pending!');
            }
        }

        $status = 0;
        $status_message = '';

        if($request->has('approved'))
        {
            $status = 2;
            $status_message = 'approved';
        }
        elseif($request->has('denied'))
        {
            $status = 4;
            $statstatus_messages = 'denied';
        }

        ServiceRequest::whereId($id)->update([
            'request_status_id'=>$status
        ]);
        ServiceRequestItem::whereRequestId($id)->whereRequestBrandStatusId(1)->update([
            'request_brand_status_id'=>$status
        ]);
        ServiceRequestRemarks::create([
            'request_id' => $id,
            'user_id' => Auth::user()->id,
            'remarks' => $request->remarks
        ]);

        return redirect()->route('service_request_items', $id)->with('success', 'Service Request successfuly ' . $status_message . '!');
    }

    public function assign_item_request($request_id, $brand_id, $category_id)
    {
        $id = $request_id;
        $request = ServiceRequest::whereId($request_id)->first();
        $request_item = ServiceRequestItem::whereRequestId($request_id)->whereItemBrandId($brand_id)->whereItemCategoryId($category_id)->first();
        #$rows = ServiceRequestItem::whereId($request_item_id)->get();
        $rows = Item::whereItemBrandId($brand_id)->whereItemCategoryId($category_id)->get();
        $from = date('Y-m-d H:i:s', strtotime(trim($request->request_datetime)));
        $to = date('Y-m-d H:i:s', strtotime(trim($request->return_datetime)));

        $data = compact('id', 'brand_id', 'category_id', 'request', 'request_item', 'rows', 'from', 'to');

        return view('request.assign', $data);
    }

    public function assign_item_request_submit(Request $request, $request_id, $brand_id, $category_id)
    {
        $this->validate(
            $request,
            [
                'item' => 'required'
            ],
            [
                'item.required' => 'Item is required!'
            ]
        );

        $row = ServiceRequestItem::whereRequestId($request_id)->whereItemBrandId($brand_id)->whereItemCategoryId($category_id)->first();

        #echo count($request->item) . ' = ' . $row->quantity . '<br /><br />';
        #dd($row);

        if(count($request->item) > $row->quantity)
        {
            return  redirect()->route('service_request_assign_items', [
                        'request_id'=>$request_id, 
                        'brand_id'=>$brand_id, 
                        'category_id'=>$category_id
                    ])
                    ->with('custom_error', 'Quantity should not exceed to ' . $row->quantity . '!');
        }

        foreach ($request->item as $key => $value)
        {
            ServiceRequestBorrowedItem::create([
                'request_id' => $request_id,
                'request_item_id' => $row->id,
                'item_id' => $key,
                'user_id' => Auth::user()->id,
            ]);
            Item::whereId($key)->update(['status'=>2]);
        }

        ServiceRequestItem::whereId($row->id)->update(['request_brand_status_id'=>2]);

        #dd($request->all());

        return  redirect()->route('service_request_assign_items', [
                    'request_id'=>$request_id, 
                    'brand_id'=>$brand_id, 
                        'category_id'=>$category_id
                ])
                ->with('success', 'Item successfully assigned!');
    }

    public function request_return($id)
    {
        $sr = ServiceRequest::whereId($id)->first();
        $sr_rows = ServiceRequestItem::whereRequestId($id)->get();

        $data = compact('id', 'sr', 'sr_rows');

        return view('request.return', $data);
    }

    public function request_receipt($id)
    {
        $sr = ServiceRequest::whereId($id)->first();
        $sr_rows = ServiceRequestItem::whereRequestId($id)->get();

        $data = compact('id', 'sr', 'sr_rows');

        return view('reports.receipt', $data);
    }

    public function request_closed($id)
    {
        $sr = ServiceRequest::where('id', function($query) use ($id){
                $query->select('request_id')->from('request_item')->whereId($id)->first();
        })->first();

        foreach(ServiceRequestBorrowedItem::whereRequestId($id)->get() as $row)
        {
            Item::whereId($row->item_id)->whereStatus(2)->update(['status'=>1]);
        }

        ServiceRequestItem::whereId($id)->update(['request_brand_status_id'=>5]);

        $total_count = ServiceRequestItem::whereRequestId($id)->whereRequestBrandStatusId(2)->count();

        if($total_count == 0)
        {
            ServiceRequest::whereId($sr->id)->update(['request_status_id'=>5]);
            return redirect()->route('service_request_return', $sr->id)->with('success', 'Service Request successfully closed!');
        }

        return redirect()->route('service_request_return', $sr->id)->with('success', 'Service Request Item successfully closed!');
    }

    public function request_deny($id)
    {
        if(ServiceRequest::whereId($id)->count() == 0)
        {
            return redirect()->route('service_request')->with('custom_error', 'Service Request does not exists!');
        }

        ServiceRequest::whereId($id)->update(['request_status_id'=>4]);
        ServiceRequestItem::whereRequestId($id)->update(['request_brand_status_id'=>4]);

        return redirect()->route('service_request')->with('success', 'Service Request has denied successfuly!');
    }

    public function request_cancel($id)
    {
        if(ServiceRequest::whereId($id)->count() == 0)
        {
            return redirect()->route('service_request')->with('custom_error', 'Service Request does not exists!');
        }

        ServiceRequest::whereId($id)->update(['request_status_id'=>3]);

        return redirect()->route('service_request')->with('success', 'Service Request cancelled successfuly!');
    }

    public function item_cancel($id)
    {
        $row = ServiceRequestItem::whereId($id)->first();

        if(is_null($row))
        {
            return redirect()->route('service_request')->with('custom_error', 'Item does not exists!');
        }

        ServiceRequestItem::whereId($id)->update(['request_brand_status_id'=>3]);

        return redirect()->route('service_request_items', $row->request_id)->with('success', 'Item cancelled successfuly!');
    }
    public function item_deny($id)
    {
        $row = ServiceRequestItem::whereId($id)->first();

        if(is_null($row))
        {
            return redirect()->route('service_request')->with('custom_error', 'Item does not exists!');
        }

        ServiceRequestItem::whereId($id)->update(['request_brand_status_id'=>4]);

        return redirect()->route('service_request_items', $row->request_id)->with('success', 'Item denied successfuly!');
    }

    public function notification()
    {
        $service_request = ServiceRequest::with('user')->whereRequestStatusId(1)->orderBy('request_datetime', 'ASC')->get();
        $data = compact('service_request');
        return response()->json($data);
    }

    public function return_item_request($request_borrowed_item_id)
    {
        $action = route('service_request_return_item_submit', $request_borrowed_item_id);
        $row = ServiceRequestBorrowedItem::whereId($request_borrowed_item_id)->first();
        $sr = ServiceRequest::whereId($row->request_id)->first();
        $return_status = ServiceRequestReturnItemStatus::get();
        $request_return_status_id = old('request_return_status_id');
        $remarks = old('remarks');

        $data = compact('action', 'sr', 'row', 'return_status', 'request_return_status_id', 'remarks');

        return view('request.return-form', $data);
    }

    public function return_item_request_submit(Request $request, $request_borrowed_item_id)
    {
        $input = $request->only('request_id', 'request_borrowed_item_id', 'request_return_status_id', 'remarks');
        $input['user_id'] = Auth::user()->id;
        $this->validate(
            $request,
            [
                'request_id' => 'required',
                'request_borrowed_item_id' => 'required',
                'request_return_status_id' => 'required',
                'remarks' => 'required'
            ],
            [
                'request_return_status_id.required' => 'Status is required!',
                'remarks.required' => 'Remarks is required!'
            ]
        );
        
        ServiceRequestReturnItem::create($input);

        if($request->request_return_status_id == 1)
        {
            #ServiceRequestItem::whereId($request_borrowed_item_id)->update([ 'request_brand_status_id'=>5 ]);
        }
        else
        {
            Item::whereId($request->request_item_id)->update(['status'=>3]);
            #ServiceRequestItem::whereId($request_borrowed_item_id)->update([ 'request_brand_status_id'=>6 ]);
        }

        return redirect()->route('service_request_return', $request->request_id)->with('success', 'Item successfuly recorded!');
    }
}
