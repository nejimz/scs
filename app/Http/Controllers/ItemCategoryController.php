<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\ItemCategory;
use Validation;
use DB;

class ItemCategoryController extends Controller
{
    public function index()
    {
    	$name = old('name');

    	$rows = ItemCategory::orderBy('name','ASC')->get();

    	$data = compact('name', 'rows');

    	return view('item.category.index', $data);
    }

    public function create()
    {
    	$action = route('item_category_store');
    	$name = old('name');

    	$data = compact('action', 'name');

    	return view('item.category.form', $data);
    }

    public function store(Request $request)
    {
    	$input = $request->only('name');
    	$this->validate(
    		$request,
    		[
    			'name' => 'required|unique:item_category,name'
    		],
    		[
    			'name.required'=>'Name is required!'
    		]
    	);

    	ItemCategory::create($input);

    	return redirect()->route('item_category_create')->with('success', 'Item Category successfuly added!');
    }

    public function edit($item_category_id)
    {
    	$action = route('item_category_update', $item_category_id);
    	$row = ItemCategory::whereId($item_category_id)->first();

    	$name = $row->name;

    	$data = compact('action', 'name');
    	
    	return view('item.category.form', $data);
    }

    public function update($item_category_id, Request $request)
    {
    	$input = $request->only('name');
    	$this->validate(
    		$request,
    		[
    			'name' => 'required|unique:item_category,name,' . $item_category_id.'.id'
    		],
    		[
    			'name.required'=>'Name is required!'
    		]
    	);

    	ItemCategory::whereId($item_category_id)->update($input);
    	return redirect()->route('item_category_edit', $item_category_id)->with('success', 'Item Category successfuly updated!');
    }

}
