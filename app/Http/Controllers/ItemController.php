<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\ItemBrand;
use App\ItemCategory;
use App\ServiceRequestItem;
use DB;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        $page_number = 30;
        $search = '';
        $name = old('name');

        if($request->has('search'))
        {
            $search = trim($request->search);
            $items = Item::where('isbn', 'LIKE', "%$search%")->orWhere('name', 'LIKE', "%$search%")->orderBy('name', 'ASC')->paginate($page_number);
        }
        else
        {
            $items = Item::orderBy('name', 'ASC')->paginate($page_number);
        }

        $data = compact('name', 'search', 'items');

        return view('item.index', $data);
    }

    public function qrprint($item_id)
    {
        $isbn = Item::find($item_id)->isbn;
        return view('reports.qrcode', compact('isbn', 'item_id'));
    }

    public function create()
    {
        $action = route('item_store');
        $isbn = old('isbn');
        $name = old('name');
        $item_brand_id = old('item_brand_id');
        $item_category_id = old('item_category_id');
        $brand = ItemBrand::get();
        $category = ItemCategory::get();

        $data = compact('action', 'isbn', 'name', 'item_brand_id', 'item_category_id', 'brand', 'category');

        return view('item.form', $data);
    }

    public function store(Request $request)
    {
        //dd($request);
        $input = $request->only('isbn', 'name', 'item_brand_id', 'item_category_id');
        $input['status'] = 1;
        #dd($input);
        $this->validate(
            $request,
            [
                'isbn' => 'required|unique:item,isbn',
                'name' => 'required|unique:item,name',
                'item_brand_id' => 'required',
                'item_category_id' => 'required'
            ],
            [
                'isbn.required'=>'ISBN is required!',
                'name.required'=>'Name is required!',
                'item_brand_id.required'=>'Brand is required!',
                'item_category_id.required'=>'Category is required!'
            ]
        );

        Item::create($input);
        return redirect()->route('item_create')->with('success', 'Item successfuly added!');
    }

    public function edit($item_id)
    {
        $action = route('item_update', $item_id);
        $row = Item::whereId($item_id)->first();
        $isbn = $row->isbn;
        $name = $row->name;
        $item_brand_id = $row->item_brand_id;
        $item_category_id = $row->item_category_id;
        $brand = ItemBrand::get();
        $category = ItemCategory::get();

        $data = compact('action', 'item_id', 'isbn', 'name', 'item_brand_id', 'item_category_id', 'brand', 'category');

        
        return view('item.form', $data);
    }

    public function update($item_id, Request $request)
    {
         //dd($request);
        $input = $request->only('isbn', 'name', 'item_brand_id', 'item_category_id');
        $this->validate(
            $request,
            [
                'isbn' => 'required|unique:item,isbn,' . $item_id . ',id',
                'name' => 'required|unique:item,name,' . $item_id . ',id',
                'item_brand_id' => 'required',
                'item_category_id' => 'required'
            ],
            [
                'isbn.required'=>'ISBN is required!',
                'name.required'=>'Name is required!',
                'item_brand_id.required'=>'Brand is required!',
                'item_category_id.required'=>'Category is required!'
            ]
        );

        Item::whereId($item_id)->update($input);
        return redirect()->route('item_edit', $item_id)->with('success', 'Item successfuly updated!');
    }

    public function search(Request $request)
    {
        $data = [];

        if($request->has('search'))
        {
            $search = trim($request->search);
            $from = date('Y-m-d H:i:s', strtotime(trim($request->request_datetime)));
            $to = date('Y-m-d H:i:s', strtotime(trim($request->return_datetime)));

            #$rows = ItemBrand::where('name', 'LIKE', "%$search%")->take(100)->get();
            $rows = Item::select(DB::raw('item.id AS item_id'), 'item_category_id', 'item_brand_id', DB::raw('item.name AS item_name'), DB::raw('item_category.name AS item_category_name'), DB::raw('item_brand.name AS item_brand_name'))
            ->leftJoin('item_category', 'item.item_category_id', '=', 'item_category.id')
            ->leftJoin('item_brand', 'item.item_brand_id', '=', 'item_brand.id')
            #->where('item.name', 'LIKE', "%$search%")
            ->where('item_category.name', 'LIKE', "%$search%")
            ->orWhere('item_brand.name', 'LIKE', "%$search%")
            ->take(100)
            ->groupBy('item_brand_id')
            ->orderBy('item.name', 'ASC')
            ->get();
            
            #echo '<pre>';dd($rows);echo '</pre>';

            foreach($rows as $row)
            {
                $total_quantity = Item::where('item_category_id', $row->item_category_id)->where('item_brand_id', $row->item_brand_id)->count();

                $items = ServiceRequestItem::leftJoin('request', 'request.id', '=', 'request_item.request_id')
                ->where('request_brand_status_id', 2)
                ->where('request_status_id', 2)
                ->where('item_category_id', $row->item_category_id)
                ->where('item_brand_id', $row->item_brand_id)
                ->get();

                foreach ($items as $item)
                {
                    if($item->request_datetime <= $from && $item->return_datetime >= $from)
                    {
                        $total_quantity -= $item->quantity;
                    }
                    elseif($item->request_datetime <= $to && $item->return_datetime >= $to)
                    {
                        $total_quantity -= $item->quantity;
                    }
                    elseif($from <= $item->request_datetime && $to >= $item->request_datetime)
                    {
                        $total_quantity -= $item->quantity;
                    }
                    elseif($from <= $item->return_datetime && $to >= $item->return_datetime)
                    {
                        $total_quantity -= $item->quantity;
                    }
                }

                $data[$row->item_id . '-' . $total_quantity] = $row->item_category_name . '(' . $row->item_brand_name . ') ' . $total_quantity . '';
                /*$quantity = $row->total_qty;

                $brands = ItemBrand::select('item_brand_id', 'item_brand.name AS item_name', 'request_datetime', 'return_datetime', 'quantity')
                ->leftJoin('request_item', 'request_item.item_brand_id', '=', 'item_brand.id')
                ->leftJoin('request', 'request.id', '=', 'request_item.request_id')
                ->where('request_brand_status_id', 2)
                ->where('request_status_id', 2)
                ->get();

                foreach ($brands as $brand)
                {
                    if($brand->request_datetime <= $from && $brand->return_datetime >= $from)
                    {
                        $quantity = $quantity - $brand->quantity;
                    }
                    elseif($brand->request_datetime <= $to && $brand->return_datetime >= $to)
                    {
                        $quantity = $quantity - $brand->quantity;
                    }
                    elseif($from <= $brand->request_datetime && $to >= $brand->request_datetime)
                    {
                        $quantity = $quantity - $brand->quantity;
                    }
                    elseif($from <= $brand->return_datetime && $to >= $brand->return_datetime)
                    {
                        $quantity = $quantity - $brand->quantity;
                    }
                }

                $data[$row->id] = $row->name . '(' . $quantity . ')';
                */
            }

            /*$rows = Item::select('item_id', 'isbn', 'item.name AS item_name', 'request_datetime', 'return_datetime')
            ->leftJoin('item_category', 'item_category.id', '=', 'item.item_category_id')
            ->leftJoin('request_item', 'request_item.item_id', '=', 'item.id')
            ->leftJoin('request', 'request_item.request_id', '=', 'request.id')
            ->where('request_status_id', 2)
            ->where('request_status_id', 2)
            ->where(function($query) use ($search){
                $query->where('item.name', 'LIKE', "%$search%")->orWhere('item_category.name', 'LIKE', "%$search%");
            })
            ->orderBy('item.name', 'ASC')
            ->lists('item_name', 'item_id');
            $data = $rows;*/
            #dd($rows);

            /*foreach($rows as $row)
            {
                $label = '' . $row->name;
                #$service_request = ServiceRequest::whereRequestStatusId(1)->orderBy('request_datetime', 'ASC')->get();
                $service_request = ServiceRequestItem::whereItemId($row->id)
                ->whereRequestStatusId(1)
                ->orderBy('request_datetime', 'ASC')->get();

                foreach($service_request as $row)
                {

                    #echo $row->request_datetime . ' >= ' . $from /. ' && ' . $row->return_datetime . ' <= ' . $from . '<br />';
                    #echo $row->request_datetime . ' >= ' . $to . ' && ' . $row->return_datetime . ' <= ' . $to . '<br /><br />';
                    if($row->request_datetime <= $from && $row->return_datetime >= $from)
                    {
                        return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
                    }
                    elseif($row->request_datetime <= $to && $row->return_datetime >= $to)
                    {
                        return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
                    }
                    elseif($from <= $row->request_datetime && $to >= $row->request_datetime)
                    {
                        return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
                    }
                    elseif($from <= $row->return_datetime && $to >= $row->return_datetime)
                    {
                        return redirect()->route('service_request_create')->withInput()->with('custom_error', 'Item has already been scheduled!');
                    }
                }

                $data[$row->id] = $label;
            }*/
        }

        return response()->json($data);
    }

}
