<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use App\ServiceRequest;
use App\Item;
use App\User;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
            if(Auth::user()->role == 'admin')
            {
                 $high_demand_items = [];
        $items = \App\ItemBrand::with('requests')->get();
        foreach($items as $item)
        {
            if(is_null($item->item))
            {
                if($item->requests->count()>0)
                {
                    $high_demand_items[] = ['brand_id' => $item->id, 'supply' => 0, 'demand' => 0]; 
                }
            }
            else
            {
                //$supply = $item->item->where('status','!=',3);
                if(Item::division($item->item->count() , $item->requests->count()) < 1)
                {
                    $high_demand_items[] = ['brand_id' => $item->id, 'supply' => $item->items->count(), 'demand' => $item->requests->count()]; 
                }
            }
        }

                $annual_requests = ServiceRequest::selectRaw("MONTHNAME(created_at) AS name,  COUNT(*) AS y")->whereYear('created_at','=', date('Y'))->groupBy(DB::raw('MONTHNAME(created_at)'))->get();

                $most_requested = ServiceRequest::selectRaw("item_brand.name AS name, COUNT(request_item.quantity) AS y, item_brand.id url")
                    ->leftJoin('request_item', 'request_item.request_id', '=','request.id')
                    ->leftJoin('item_brand', 'item_brand.id', '=','request_item.item_brand_id')
                    ->whereYear('request.created_at','=', date('Y'))
                    ->groupBy('item_brand.name')->orderBy(DB::raw('COUNT(request_item.quantity)'), "DESC")
                    ->limit(10)->get();

                // $least_requested = ServiceRequest::selectRaw("item_brand.name As name, COUNT(*) AS y")
                //     ->leftJoin('request_item', 'request_item.request_id', '=','request.id')
                //     ->leftJoin('item_brand', 'item_brand.id', '=','request_item.item_brand_id')
                //     ->whereYear('request.created_at','=', date('Y'))
                //     ->groupBy('item_brand.name')->orderBy(DB::raw('COUNT(*)'), "ASC")
                //     ->limit(10)->get();

                $most_user = User::selectRaw('users.name, COUNT(*) y, users.id url') 
                            ->leftJoin('request', 'request.user_id', '=','users.id')
                            ->whereYear('request.created_at','=', date('Y'))
                            ->limit(10)->get();
                //$snd = collect(['Some items are in need of purchase due to demand']);
                $data = compact('annual_requests', 'most_requested', 'most_user', 'high_demand_items');
                return view('home', $data);
            }
            else
            {
                return view('welcome');   
            }
        }
        else
        {
            return view('welcome');
        }   

    }

    public function  purchasedss()
    {
        $high_demand_items = [];
        $items = \App\ItemBrand::with('requests')->get();
        foreach($items as $item)
        {
            //dump();
            if(is_null($item->items))
            {
                if($item->requests->count()>0)
                {
                    $high_demand_items[] = ['brand_id' => $item->id, 'supply' => 0, 'demand' => 0]; 
                }
            }
            else
            {
                //$supply = $item->item->where('status','!=',3);
                if(Item::division($item->items->count(), $item->requests->count()) < 1)
                {
                    $high_demand_items[] = ['brand_id' => $item->id, 'supply' => $item->items->count(), 'demand' => $item->requests->count()]; 
                }
            }
        }
        $data = compact('items', 'high_demand_items');
        return view('reports.dss.purchase', $data);
    }

    public function userdss($id)
    {
        $users = User::with(['requests' => function($query){
                                $query ->whereYear('created_at','=', date('Y'));
                            }])->where('users.id', $id)
                            ->first();
        $data = compact('users');
        return view('reports.dss.user', $data);
    }

    public function itemdss($id)
    {
        $items = ServiceRequest::leftJoin('request_item', 'request_item.request_id', '=','request.id')
                    ->leftJoin('item_brand', 'item_brand.id', '=','request_item.item_brand_id')
                    ->where('request_item.item_brand_id', $id)
                    ->whereYear('request.created_at','=', date('Y'))
                    ->orderBy('request.created_at', "DESC")
                    ->get();
        $data = compact('items');
        return view('reports.dss.item', $data);
    }
}
