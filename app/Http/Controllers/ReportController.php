<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\User;
use App\ServiceRequest;
use DB;

class ReportController extends Controller
{
	public function index(Request $request)
    {
    	return view('reports.index');
    }

    public function generate(Request $request)
    {
    	$report_title = $request->report_type;
    	$report_date_start = date("Y-m-d 00:00:00", strtotime($request->report_date_start));
    	$report_date_end = date("Y-m-d 23:59:59", strtotime($request->report_date_end));
    	//$report_date_array = explode("/", $request->report_date);

    	if($report_title == 'items')
    	{
	    	$brands = Item::selectRaw("item_brand_id, 
	    		SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS available_count, 
	    		SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS defective_count, 
	    		SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS borrowed_count, COUNT(*) AS total_count")
	    	->whereBetween('created_at',[$report_date_start, $report_date_end])
	    	->groupBy('item_brand_id')->get();

	    	//dd($brands);

	    	$categories = Item::selectRaw("item_category_id, 
	    		SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS available_count, 
	    		SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS defective_count, 
	    		SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS borrowed_count, COUNT(*) AS total_count")
	    	->whereBetween('created_at',[$report_date_start, $report_date_end])
	    	->groupBy('item_category_id')->get();

	    	$items = Item::whereBetween('created_at',[$report_date_start, $report_date_end])
	    	->orderBy('created_at', 'DESC')
	    	->get();

	    	// $items_available = Item::selectRaw('*, count(*) as total')->whereStatus(1)->groupBy('item_brand_id')->get();
	    	// $items_defective = Item::selectRaw('*, count(*) as total')->whereStatus(3)->groupBy('item_brand_id')->get();
	    	// $items_borrowed = Item::selectRaw('*, count(*) as total')->whereStatus(2)->groupBy('item_brand_id')->get();
	    	//$data = compact('report_title', 'brands', 'categories', 'items_available', 'items_defective', 'items_borrowed');
	    	$data = compact('report_title', 'report_date_start', 'report_date_end', 'brands', 'categories', 'items');
	    }
	    elseif($report_title == 'users')
	    {
	    	$roles = User::selectRaw("role,
	    		SUM(CASE WHEN active = 1 THEN 1 ELSE 0 END) AS active_count, 
	    		SUM(CASE WHEN active = 0 THEN 1 ELSE 0 END) AS inactive_count, 
	    		COUNT(*) AS total_count")
	    	->whereBetween('created_at',[$report_date_start, $report_date_end])
	    	->groupBy('role')->get();

	    	$users = User::orderBy('email')
	    	->whereNotBetween('created_at',[$report_date_start, $report_date_end])
	    	->get();
	    	$data = compact('report_title', 'report_date_start', 'report_date_end', 'roles', 'users');
	    }
	    elseif($report_title == 'requests')
	    {
	    	$most_requested = ServiceRequest::selectRaw("item_brand.name AS name, COUNT(request_item.quantity) AS y")
                    ->leftJoin('request_item', 'request_item.request_id', '=','request.id')
                    ->leftJoin('item_brand', 'item_brand.id', '=','request_item.item_brand_id')
                    ->whereBetween('request.created_at',[$report_date_start, $report_date_end])
                    ->groupBy('item_brand.name')->orderBy(DB::raw('COUNT(request_item.quantity)'), "DESC")
                    ->limit(10)
                    ->get();
            $most_user = User::selectRaw('users.name, COUNT(*) y') 
                            ->leftJoin('request', 'request.user_id', '=','users.id')
                    		->whereBetween('request.created_at',[$report_date_start, $report_date_end])
                    		->limit(10)
                            ->get();

            $request_summary = ServiceRequest::selectRaw("
	    		SUM(CASE WHEN request_status_id = 1 THEN 1 ELSE 0 END) AS pending_count, 
	    		SUM(CASE WHEN request_status_id = 2 THEN 1 ELSE 0 END) AS approved_count, 
	    		SUM(CASE WHEN request_status_id = 3 THEN 1 ELSE 0 END) AS cancelled_count, 
	    		SUM(CASE WHEN request_status_id = 4 THEN 1 ELSE 0 END) AS denied_count, 
	    		SUM(CASE WHEN request_status_id = 5 THEN 1 ELSE 0 END) AS closed_count, 
	    		COUNT(*) AS total_count")
	    		->whereBetween('created_at',[$report_date_start, $report_date_end])->get();

            $requests_lists = ServiceRequest::whereBetween('created_at',[$report_date_start, $report_date_end])->get();

            $data = compact('report_title', 'report_date_start', 'report_date_end', 'request_summary', 'most_requested', 'most_user', 'requests_lists');
	    }
	    else
	    {
	    	abort(404);
	    }

    	return view('reports.generate', $data);
    }
}
