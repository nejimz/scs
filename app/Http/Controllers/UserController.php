<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\ServiceRequestBorrowedItem;
use App\ServiceRequestReturnItem;
use App\Position;
use App\Profile;
use Auth;
use Validator;
use Hash;

class UserController extends Controller
{
    public function index(Request $request) 
    {
    	$page_number = 30;
        $search = '';
        $name = old('name');

        if($request->has('search'))
        {
            $search = trim($request->search);
            $users = User::where('email', 'LIKE', "%$search%")->orWhere('name', 'LIKE', "%$search%")->paginate($page_number);
        }
        else
        {
            $users = User::paginate($page_number);
        }

        $data = compact('name', 'search', 'users');

        return view('users.index', $data);
    }    

    public function create()
    {
        $action = route('user_store');
        $email = old('email');
        $name = old('name');
        $role = 'basic';
        $active = 1;
        $data = compact('action', 'email', 'name', 'role', 'active');
        return view('users.form', $data);
    }

    public function store(Request $request)
    {
        $request['password'] = bcrypt($request['password']);
        $input = $request->only('name', 'email', 'password', 'role', 'active');
        //dd($input);
        $this->validate(
            $request,
            [
                'email' => 'required|unique:users,email',
                'name' => 'required|unique:users,name',
                'password' => 'required'
            ],
            [
                'required'=>':attribute is required!'
            ]
        );

        User::create($input);

        return redirect()->route('user_create')->with('success', 'User Successfuly Created!');
    }
    public function edit($user_id)
    {
        $action = route('user_update', $user_id);
        $positions = Position::get();

        $row = User::whereId($user_id)->first();
        $email = $row->email;
        $name = $row->name;
        $role = $row->role;
        $active = $row->active;

        $profile = Profile::whereUserId($user_id)->first();
        if(!is_null($profile))
        {
            $user_position = $profile->position_id;
            $first_name = $profile->first_name;
            $middle_name = $profile->middle_name;
            $last_name = $profile->last_name;
            $address = $profile->address;
            $school_id = $profile->school_id;
            $date_of_birth = $profile->date_of_birth;
            $grade  = $profile->grade;
            $section = $profile->section;
            $date_hired = $profile->date_hired;
            $educational_background = $profile->educational_background;
        }

        $data = compact('action', 'email', 'name', 'user_id', 'role', 'active', 'user_position', 'positions', 'first_name', 'middle_name', 'last_name', 'address', 'school_id', 'date_of_birth', 'grade', 'section', 'date_hired', 'educational_background');

        return view('users.form', $data);
    }

    public function profile()
    {
        $action = route('user_profile_update', Auth::id());
        $row = User::whereId(Auth::id())->first();

        $email = $row->email;
        $name = $row->name;
        $profile = Profile::whereUserId($row->id)->first();
        if(!is_null($profile))
        {
            $user_position = $profile->position_id;
            $first_name = $profile->first_name;
            $middle_name = $profile->middle_name;
            $last_name = $profile->last_name;
            $address = $profile->address;
            $school_id = $profile->school_id;
            $date_of_birth = $profile->date_of_birth;
            $grade  = $profile->grade;
            $section = $profile->section;
            $date_hired = $profile->date_hired;
            $educational_background = $profile->educational_background;
        }

        $data = compact('action', 'email', 'name', 'role', 'active', 'user_position', 'positions', 'first_name', 'middle_name', 'last_name', 'address', 'school_id', 'date_of_birth', 'grade', 'section', 'date_hired', 'educational_background');

        return view('profile', $data);
    }

    public function update_profile($user_id, Request $request)
    {
        Validator::extend('user_password', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password);
        });

        if($request->has('password'))
        {
            $this->validate(
                $request,
                [
                    'name' => 'required|unique:users,name,' . $user_id . ',id',
                    'email' => 'required|unique:users,email,' . $user_id . ',id',
                    'password_old' => 'required|user_password',
                    'password' => 'confirmed'
                ],
                [
                    'required'=>':attribute is required!',
                    'password_old.required'=>'Old password is required!',
                    'confirmed'=>'Password and confirm password did not match'
                ]
            );
            $request['password'] = bcrypt($request['password']);
            $input = $request->only('email', 'name', 'password');
        }
        else
        {
            $input = $request->only('email', 'name');
            $this->validate(
                $request,
                [
                    'name' => 'required|unique:users,name,' . $user_id . ',id',
                    'email' => 'required|unique:users,email,' . $user_id . ',id'
                ],
                [
                    'required'=>':attribute is required!'
                ]
            );
        }

        User::whereId($user_id)->update($input);
        return redirect()->route('user_profile', $user_id)->with('success', 'Profile Successfuly Update!');
    }

    public function update($user_id, Request $request)
    {
        $this->validate(
            $request,
            [
                'school_id' => 'unique:profiles,school_id,' . $user_id . ',user_id',
                'name' => 'required|unique:users,name,' . $user_id . ',id',
                'email' => 'required|unique:users,email,' . $user_id . ',id',
                'password' => 'confirmed',
            ],
            [
                'unique'=>':attribute is already in use',
                'required'=>':attribute is required!',
                'confirmed'=>'Password and confirm password did not match'
            ]
        );

        if($request->has('password'))
        {
            $request['password'] = bcrypt($request['password']);
            $input = $request->only('email', 'name', 'password', 'active', 'role');
        }
        elseif($request->has('reset'))
        {
            $request['password'] = bcrypt($request['password_reset']);
            $request['reset'] = 1;
            $input = $request->only('password','reset');
        }
        else
        {
            $input = $request->only('email', 'name', 'active', 'role');
        }

        User::whereId($user_id)->update($input);

  

        if(Profile::where('user_id',$user_id)->count() == 0)
        {
             $request['user_id'] = $user_id;

            $profile_input = $request->only('position_id', 'user_id', 'first_name', 'middle_name', 'last_name', 'address', 'school_id', 'date_of_birth', 'grade', 'section', 'date_hired', 'educational_background');
            Profile::create($profile_input);
        }
        else
        {
            $profile_input = $request->only('position_id', 'first_name', 'middle_name', 'last_name', 'address', 'school_id', 'date_of_birth', 'grade', 'section', 'date_hired', 'educational_background');
            Profile::where('user_id',$user_id)->update($profile_input);
        }

        if($request->has('reset'))
        {
            return redirect()->route('user_edit', $user_id)->with('success', 'User has been updated!')->withInput($request->only('password_reset'));
        }
        else
        {
           return redirect()->route('user_edit', $user_id)->with('success', 'User has been updated!');
        }
        
        
    }

    public function penalties($id)
    {
        $rows = ServiceRequestReturnItem::leftJoin('request', 'request_return_item.request_id', '=', 'request.id')
        ->leftJoin('request_borrowed_item', 'request_return_item.request_borrowed_item_id', '=', 'request_borrowed_item.id')
        ->leftJoin('item', 'request_borrowed_item.item_id', '=', 'item.id')
        ->leftJoin('item_brand', 'item.item_brand_id', '=', 'item_brand.id')
        ->leftJoin('item_category', 'item.item_category_id', '=', 'item_category.id')
        ->leftJoin('request_return_item_status', 'request_return_item.request_return_status_id', '=', 'request_return_item_status.id')
        ->select('isbn', 'item.name AS item_name', 'item_brand.name AS brand_name', 'item_category.name AS category_name', 'request_return_item_status.name AS status_name')
        ->where('request.user_id', $id)
        ->where('request_return_item.request_return_status_id', 2)
        ->paginate(30);

        $data = compact('rows');

        return view('users.penalties', $data);
    }

    public function reset_user_password(Request $request)
    {
        return view('auth.reset', compact('request'));
    }

    public function reset_user_confirm(Request $request)
    {
        $this->validate(
                $request,
                [
                    'password' => 'required|confirmed'
                ],
                [
                    'required'=>'Password is required',
                    'confirmed'=>'Password did not match'
                ]
            );

            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->password);
            $user->reset = 0;
            $user->save();
            return redirect('/');
    }
}
