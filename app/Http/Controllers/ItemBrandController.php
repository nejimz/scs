<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\ItemBrand;
use Validation;
use DB;

class ItemBrandController extends Controller
{
    public function index()
    {
    	$name = old('name');

    	$rows = ItemBrand::orderBy('name', 'ASC')->get();

    	$data = compact('name', 'rows');

    	return view('item.brand.index', $data);
    }

    public function create()
    {
    	$action = route('item_brand_store');
    	$name = old('name');

    	$data = compact('action', 'name');

    	return view('item.brand.form', $data);
    }

    public function store(Request $request)
    {
    	$input = $request->only('name');
    	$this->validate(
    		$request,
    		[
    			'name' => 'required|unique:item_brand,name'
    		],
    		[
    			'name.required'=>'Name is required!'
    		]
    	);

    	ItemBrand::create($input);

    	return redirect()->route('item_brand_create')->with('success', 'Item Brand successfuly added!');
    }

    public function edit($item_brand_id)
    {
    	$action = route('item_brand_update', $item_brand_id);
    	$row = ItemBrand::whereId($item_brand_id)->first();

    	$name = $row->name;

    	$data = compact('action', 'name');
    	
    	return view('item.brand.form', $data);
    }

    public function update($item_brand_id, Request $request)
    {
    	$input = $request->only('name');
    	$this->validate(
    		$request,
    		[
    			'name' => 'required|unique:item_brand,name,' . $item_brand_id.'.id'
    		],
    		[
    			'name.required'=>'Name is required!'
    		]
    	);

    	ItemBrand::whereId($item_brand_id)->update($input);
    	return redirect()->route('item_brand_edit', $item_brand_id)->with('success', 'Item Brand successfuly updated!');
    }

}
