<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\ServiceRequestReturnItem;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'active', 'reset'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setActiveAttribute($value)
    {
        return $value == 'on' ? 1 : 0 ;
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'id', 'user_id');
    }

    public function requests()
    {
        return $this->hasMany('App\ServiceRequest');
    }

    public function getActiveStatusAttribute($value)
    {
        return ($this->active == 1) ? "Yes" : "No";
    }

    public function getRoleAliasAttribute($value)
    {
        switch ($this->role) {
            case 'admin':
                return "Administrator";
                break;
            case 'basic':
                return "Basic User";
                break;
            default:
                return "Unassigned";
                break;
        }
    }

    public function getActiveStatusLabelAttribute($value)
    {
        return ($this->active == 1) ? "<span class='label primary'>Yes</span>" : "<span class='label secondary'>No</span>";
    }

    public function getRoleAdminLabelAttribute($value)
    {
        switch ($this->role) {
            case 'admin':
                return "<span class='label primary'>Yes</span>";
                break;
            case 'basic':
                return "<span class='label secondary'>No</span>";
                break;
            default:
                return "<span class='label warning'>N/A</span>";
                break;
        }
    }

    public function getTotalPenaltiesAttribute()
    {
        $id = $this->id;

        #$count = ServiceRequestReturnItem::whereRequestReturnStatusId(2)->count();
        $count = ServiceRequestReturnItem::leftJoin('request', 'request_return_item.request_id', '=', 'request.id')
        ->where('request.user_id', $id)
        ->where('request_return_item.request_return_status_id', 2)
        ->count();

        return $count;
    }

    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
}
