<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestReturnItemStatus extends Model
{
    public $table = 'request_return_item_status';
    public $fillable = ['name'];
    public $timestamps = false;

    public function request_return_item()
    {
        return $this->hasOne('App\ServiceRequestReturnItem', 'id', 'request_return_status_id');
    }
}
