<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestBorrowedItem extends Model
{
    public $table = 'request_borrowed_item';
    public $fillable = ['request_id', 'request_item_id', 'item_id', 'user_id', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function item()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

    public function request()
    {
        return $this->hasOne('App\ServiceRequest', 'id', 'request_id');
    }

    public function request_item()
    {
        return $this->hasOne('App\ServiceRequestItem', 'id', 'request_item_id');
    }

    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }
}
