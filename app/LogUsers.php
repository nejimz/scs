<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUsers extends Model
{
	public $table = 'logs_user';

    protected $fillable = [
        'user_id', 'lilo', 'logged_on',
    ];
    
    public $timestamps = false;
}
