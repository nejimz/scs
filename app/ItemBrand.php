<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;

class ItemBrand extends Model
{
    public $table = 'item_brand';
    public $fillable = ['name', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function item()
    {
        return $this->belongsTo('App\Item', 'id', 'item_brand_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function requests()
    {
        return $this->hasMany('App\ServiceRequestItem');
    }

    public function service_request_item()
    {
        return $this->belongsTo('App\ServiceRequestItem', 'id', 'item_brand_id');
    }

    public function getTotalQtyAttribute()
    {
        return Item::whereItemBrandId($this->id)->count();
    }
}
