<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table = 'profiles';
    public $fillable = ['position_id', 'user_id', 'first_name', 'middle_name', 'last_name', 'address', 'school_id', 'date_of_birth', 'grade', 'section', 'date_hired', 'educational_background'];
    public $timestamps = true;
}
