<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ItemCategory;
use App\ItemStatus;
use DNS2D;

class Item extends Model
{
    public $table = 'item';
    public $fillable = ['isbn', 'name', 'item_brand_id', 'item_category_id', 'status', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function brand()
    {
        return $this->hasOne('App\ItemBrand', 'id', 'item_brand_id');
    }

    public function getQrcodeAttribute()
    {
        return DNS2D::getBarcodeSVG($this->isbn, "QRCODE",3,3);
    }

    public function category()
    {
        return $this->hasOne('App\ItemCategory', 'id', 'item_category_id');
    }

    public function service_request()
    {
        return $this->belongsTo('App\ServiceRequestItem', 'id', 'item_id');
    }

    public function item_status()
    {
        return $this->hasOne('App\ItemStatus', 'id', 'status');
    }

    public function getCurrentStatusAttribute()
    {
        $status = $this->status;

        $status = ItemStatus::whereId($status)->first();

        return $status->name;
    }

    public function getCreatedAtAttribute($value)
   {
        return date('Y-m-d h:iA',strtotime($value));
   }

    public static function division($a, $b) {         
        if($b === 0)
        {
          return null;
        }
        return $a/$b;
    }
}
