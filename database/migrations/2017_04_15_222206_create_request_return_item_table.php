<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestReturnItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_return_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->index();
            $table->integer('request_borrowed_item_id')->index();
            $table->integer('request_return_status_id')->index();
            $table->integer('user_id')->index();
            $table->text('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_return_item');
    }
}
