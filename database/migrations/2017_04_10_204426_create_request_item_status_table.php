<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServiceRequestItemStatus;

class CreateRequestItemStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('request_item_status'))
        {
            Schema::create('request_item_status', function (Blueprint $table) {
                $table->increments('id');
                $table->text('name');
            });

            ServiceRequestItemStatus::create(['name' => 'Pending']);
            ServiceRequestItemStatus::create(['name' => 'Approved']);
            ServiceRequestItemStatus::create(['name' => 'Cancelled']);
            ServiceRequestItemStatus::create(['name' => 'Denied']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_item_status');
    }
}
