<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServiceRequestStatus;

class Alter1RequestStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_status', function ($table) {
            $table->tinyInteger('sort');
        });

        ServiceRequestStatus::whereName('Pending')->update(['sort' => 1]);
        ServiceRequestStatus::whereName('Approved')->update(['sort' => 2]);
        ServiceRequestStatus::whereName('Closed')->update(['sort' => 3]);
        ServiceRequestStatus::whereName('Denied')->update(['sort' => 4]);
        ServiceRequestStatus::whereName('Cancelled')->update(['sort' => 5]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void 
     */
    public function down()
    {
        Schema::table('request_status', function ($table) {
            $table->dropColumn('sort');
        });
    }
}
