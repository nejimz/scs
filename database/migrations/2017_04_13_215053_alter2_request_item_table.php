<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2RequestItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->renameColumn('item_id', 'item_brand_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->renameColumn('item_brand_id', 'item_id');
            });
        }
    }
}
