<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Altem3ItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('item'))
        {
            Schema::table('item', function($table){
                $table->integer('item_brand_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('item'))
        {
            Schema::table('item', function($table){
                $table->integer('item_brand_id');
            });
        }
    }
}
