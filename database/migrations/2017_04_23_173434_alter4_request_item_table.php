<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter4RequestItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->integer('item_category_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->dropColumn('item_category_id');
            });
        }
    }
}
