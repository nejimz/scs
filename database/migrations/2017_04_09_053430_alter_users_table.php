<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users'))
        {
            Schema::table('users', function(Blueprint $table){
                $table->string('role');
                $table->tinyInteger('active');
            });
        }
        User::where('email','admin')->update(['role' => 'admin', 'active' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users'))
        {
            Schema::table('users', function(Blueprint $table){
                $table->dropColumn('role')->nullable();
                $table->dropColumn('active');
            });
        }
    }
}
