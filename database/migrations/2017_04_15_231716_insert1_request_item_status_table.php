<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServiceRequestItemStatus;

class Insert1RequestItemStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ServiceRequestItemStatus::create(['name' => 'Damage']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ServiceRequestItemStatus::whereName('Damage')->delete();
    }
}
