<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2ItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('item'))
        {
            Schema::table('item', function($table){
                $table->renameColumn('item_category', 'item_category_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('item'))
        {
            Schema::table('item', function($table){
                $table->renameColumn('item_category_id', 'item_category');
            });
        }
    }
}
