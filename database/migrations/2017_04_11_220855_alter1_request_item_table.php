<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter1RequestItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function(Blueprint $table){
                $table->integer('request_item_status_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function(Blueprint $table){
                $table->dropColumn('request_item_status_id');
            });
        }
    }
}
