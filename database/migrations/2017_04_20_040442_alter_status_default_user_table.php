<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStatusDefaultUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->integer('active')->default(1)->change();
            $table->string('role')->default('basic')->change();
            $table->integer('reset')->default(1);
        });
        DB::table('users')->update(['reset' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void 
     */
    public function down()
    {
         Schema::table('users', function ($table) {
            $table->integer('active')->default(0)->change();
            $table->string('role')->nullable()->change();
            $table->dropColumn('reset');
        });
    }
}
