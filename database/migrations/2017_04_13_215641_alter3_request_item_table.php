<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter3RequestItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->renameColumn('request_item_status_id', 'request_brand_status_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('request_item'))
        {
            Schema::table('request_item', function($table){
                $table->renameColumn('request_brand_status_id', 'request_item_status_id');
            });
        }
    }
}
