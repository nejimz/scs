<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServiceRequestReturnItemStatus;

class CreateRequestReturnItemStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_return_item_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        ServiceRequestReturnItemStatus::create(['name' => 'In Good Condition' ]);
        ServiceRequestReturnItemStatus::create(['name' => 'Damage' ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_return_item');
    }
}
