<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter1RequestBorrowedItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('request_borrowed_item'))
        {
            Schema::table('request_borrowed_item', function($table){
                $table->integer('request_item_id')->index();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('request_borrowed_item'))
        {
            Schema::table('request_borrowed_item', function($table){
                $table->dropColumn('request_item_id');
            });
        }
    }
}
