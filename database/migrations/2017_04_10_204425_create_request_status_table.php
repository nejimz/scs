<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServiceRequestStatus;

class CreateRequestStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_status', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
        });

        ServiceRequestStatus::create(['name' => 'Pending']);
        ServiceRequestStatus::create(['name' => 'Approved']);
        ServiceRequestStatus::create(['name' => 'Cancelled']);
        ServiceRequestStatus::create(['name' => 'Denied']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_status');
    }
}
