<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ItemStatus;

class CreateItemStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        ItemStatus::create(['name' => 'Available']);
        ItemStatus::create(['name' => 'Borrowed']);
        ItemStatus::create(['name' => 'Defective']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_status');
    }
}
